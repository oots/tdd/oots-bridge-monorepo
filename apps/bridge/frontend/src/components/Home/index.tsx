'use client';

import { useSearchParams } from 'next/navigation';
import { Spinner } from '@nextui-org/react';
import { useEffect, useRef, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

// Steps:
// 1. Update return URL
// 2. Redirect to provider -> map to URL
// 3. Redirect back from backend, skip frontend
export const Home = () => {
  // Search params
  const searchParams = useSearchParams();
  const formRef = useRef<HTMLFormElement | null>(null);

  // Global state
  const [step, setStep] = useState(0);

  if (!searchParams.get('sessionId')) throw new Error('Session ID not found');

  const sessionId = searchParams.get('sessionId')!;
  const returnUrl = searchParams.get('returnurl')!;

  const {
    data: sessionData,
    error: sessionError,
    isPending,
  } = useQuery<{
    error: string | null;
    providerEndpoint: string;
  }>({
    queryKey: ['sessionId', sessionId],
    queryFn: async () => {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_BRIDGE_BACKEND_URL}/session/${sessionId}`
      );
      return response.json();
    },
    enabled: !!sessionId,
  });

  useEffect(() => {
    if (!returnUrl || isPending || !sessionData) return;
    const updateReturnUrl = async () => {
      await fetch(`${process.env.NEXT_PUBLIC_BRIDGE_BACKEND_URL}/return_url`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          previewId: sessionId,
          returnUrl: returnUrl,
        }),
      });
    };

    updateReturnUrl()
      .then(() => setStep(1))
      .catch((err) => console.log(err));
  }, [returnUrl, sessionId, sessionData]);

  useEffect(() => {
    if (
      step === 1 &&
      formRef.current &&
      sessionData?.providerEndpoint &&
      !sessionData?.error
    ) {
      formRef.current.submit();
    }
  }, [step, formRef, sessionData]);

  // If there is no error, redirect to provider
  if (!sessionData?.error)
    return (
      <div className="flex flex-col mt-12 space-y-4 items-center justify-center">
        {sessionData?.providerEndpoint && (
          <div className="hidden">
            <form
              ref={formRef}
              action={sessionData?.providerEndpoint}
              method="POST"
            >
              <input
                type="hidden"
                name="sessionId"
                value={sessionId.replace(/-/g, '')}
              />
              <input
                type="hidden"
                name="returnUrl"
                value={`${process.env.NEXT_PUBLIC_BRIDGE_BACKEND_URL}/store`}
              />
            </form>
          </div>
        )}
        <Spinner />
        <h2>Redirecting</h2>
      </div>
    );

  return (
    <div className="flex flex-col mt-12 space-y-4 items-center justify-center">
      {sessionData?.error && (
        <div className="text-red-500">{sessionData?.error}</div>
      )}
      {returnUrl && (
        <div className="underline">
          <a href={returnUrl}>Go back</a>
        </div>
      )}
      {!returnUrl && <div className="text-red-500">No return URL found</div>}
      {sessionError ? (
        <div className="text-red-500">Unexpected error occured</div>
      ) : null}
    </div>
  );
};
