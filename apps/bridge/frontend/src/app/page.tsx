import { Home } from '@/components/Home';

export const dynamic = 'force-dynamic';

export default async function Page() {
  return (
    <main className="min-h-screen h-screen flex justify-center items-center">
      <div className="w-full max-w-xl">
        <h1 className="text-3xl font-bold text-center mb-4">
          OOTS Emrex Bridge
        </h1>
        <Home />
      </div>
    </main>
  );
}
