'use client';

import { Button } from '@nextui-org/react';
import { useEffect } from 'react';

export default function Page({
  error,
  reset,
}: {
  error: Error;
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <div className="h-screen w-screen flex justify-center items-center flex-col">
      <h1 className="text-3xl font-bold text-center mb-4">
        Something went wrong.
      </h1>
      <Button
        className="mb-4"
        variant="faded"
        onClick={
          // Attempt to recover by trying to re-render the segment
          () => reset()
        }
      >
        Refresh
      </Button>
      <p className="text-center mb-4">Error: {error.message}</p>
    </div>
  );
}
