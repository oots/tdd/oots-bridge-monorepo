#!/bin/sh

find /app/apps/bridge/frontend/.next \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i "s#NEXT_PUBLIC_BRIDGE_BACKEND_URL_PLACEHOLDER#$NEXT_PUBLIC_BRIDGE_BACKEND_URL#g"

exec "$@"
