# Bridge backend

This is the backend server for the Bridge.

It reads OOTS Evidence request messages from the Domibus access point and handles them.

### Health check

The health check is available at `/health`.
