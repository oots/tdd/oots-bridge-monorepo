import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  fastify.get(
    '/:id',
    {
      schema: {
        params: {
          type: 'object',
          properties: {
            id: { type: 'string' },
          },
          required: ['id'],
        },
      },
    },
    async (request, reply) => {
      const { id } = request.params;

      const previewInfo = fastify.storage.getPreviewInfo(id);

      if (!previewInfo) {
        return reply.code(404).send();
      }

      const { error, requestedEmrexDataProvider } = previewInfo.data;

      const provider =
        requestedEmrexDataProvider.SlotValue.Agent.Identifier._text;
      const providerEndpoint = fastify.config.EMREX_DATA_PROVIDERS[provider];

      if (!providerEndpoint) {
        return reply.code(500).send();
      }

      return reply.code(200).send({
        providerEndpoint,
        error,
        returnUrl: previewInfo.data.returnUrl,
      });
    }
  );
};

export default plugin;
