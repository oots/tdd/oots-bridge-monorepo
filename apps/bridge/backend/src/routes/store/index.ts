import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';
import { inflate } from 'pako';
import { fastifyFormbody } from '@fastify/formbody';
import { handlePreviewRequest } from '../request/handlePreviewRequest.js';
import { SignedXml } from 'xml-crypto';
import { DOMParser, MIME_TYPE } from '@xmldom/xmldom';
import xpath from 'xpath';
import { randomUUID } from 'node:crypto';
import { ValidationType } from '../../plugins/xsd-validator.js';

// @ts-ignore
import levenshtein from 'js-levenshtein';
import { PreviewStatus } from '../../plugins/storage.js';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  await fastify.register(fastifyFormbody, { bodyLimit: 1048576 * 10 });

  fastify.post(
    '/',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            sessionId: { type: 'string' },
            returnCode: { type: 'string' },
            elmo: { type: 'string' },
          },
          required: ['sessionId', 'returnCode'],
        },
      },
    },
    async (request, reply) => {
      const { sessionId, returnCode, elmo } = request.body;

      const uuid = sessionId.replace(
        /(.{8})(.{4})(.{4})(.{4})(.{12})/,
        '$1-$2-$3-$4-$5'
      );
      const returnUrl = fastify.storage.getReturnUrl(uuid)!;
      let previewInfo = fastify.storage.getPreviewInfo(uuid);

      if (
        ['NCP_CANCEL', 'NCP_NO_RESULTS', 'NCP_ERROR'].includes(returnCode) ||
        !elmo
      ) {
        if (returnCode === 'NCP_ERROR') {
          fastify.storage.updatePreviewInfo(uuid, {
            error: 'EMREX_ERROR_RESPONSE',
            status: PreviewStatus.Error,
          });
        }

        // Send OOTS respone
        await handlePreviewRequest(fastify, { previewId: uuid });

        // Redirect to the given url
        return reply.redirect(returnUrl);
      }

      const gzipedData = Buffer.from(elmo, 'base64');

      const data = inflate(gzipedData, { to: 'string' }); // XML String

      // EMREX XML schema validation
      const xmlValidationResult = await fastify.xsdValidator.validateXML(
        data,
        ValidationType.EMREX,
        (message: string) => {
          fastify.log.info({
            id: randomUUID(),
            messageId: null,
            conversationId:
              previewInfo?.data.domibusMessageHeaderInfo?.collaborationInfo
                ?.conversationId,
            operation: 'EMREX_XML_VALIDATION_RESULT',
            error: null,
            payload: message,
          });
        }
      );

      if (xmlValidationResult) {
        // Validate XML Signature
        const sig = new SignedXml({
          publicCert:
            fastify.config.EMREX_DATA_PROVIDERS_CERTIFICATES[
              previewInfo?.data.requestedEmrexDataProvider!.SlotValue.Agent
                .Identifier._text
            ],
        });

        const doc = new DOMParser().parseFromString(
          data,
          MIME_TYPE.XML_APPLICATION
        ) as any;

        const signature: any = xpath.select(
          "//*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']",
          doc
        );

        sig.loadSignature(signature[0]);

        try {
          const result = sig.checkSignature(data);

          if (result) {
            try {
              const learner: any = xpath.select(
                "//*[local-name(.)='learner']",
                doc,
                true
              );

              const givenNames = xpath.select(
                "//*[local-name(.)='givenNames']/text()",
                learner,
                true
              );

              const familyName = xpath.select(
                "//*[local-name(.)='familyName']/text()",
                learner,
                true
              );

              const birthDate: any = xpath.select(
                "//*[local-name(.)='bday']/text()",
                learner,
                true
              );

              if (!previewInfo?.data.userInfo.query) {
                throw new Error('User info query is not set');
              }

              const { surname, forenames, dateOfBirth } =
                previewInfo.data.userInfo.query;

              if (`${dateOfBirth}` !== `${birthDate}`) {
                throw new Error('Date of birth does not match');
              }

              const givenNameDistance = levenshtein(
                `${forenames}`,
                `${givenNames}`
              );
              if (givenNameDistance > fastify.config.LEVENSHTEIN_THRESHOLD) {
                throw new Error('Given name does not match.');
              }

              const surnameDistance = levenshtein(
                `${surname}`,
                `${familyName}`
              );
              if (surnameDistance > fastify.config.LEVENSHTEIN_THRESHOLD) {
                throw new Error('Surname does not match.');
              }

              fastify.storage.updatePreviewInfo(uuid, {
                elmo: data,
              });
            } catch (e) {
              fastify.log.error({
                id: randomUUID(),
                messageId: null,
                conversationId:
                  previewInfo?.data.domibusMessageHeaderInfo?.collaborationInfo
                    ?.conversationId,
                operation: 'IDENTITY_MATCHING_ERROR',
                error: JSON.stringify(e),
              });
              fastify.storage.updatePreviewInfo(uuid, {
                elmo: null,
                error: 'IDENTITY_MATCHING_ERROR',
                status: PreviewStatus.Error,
              });
            }
          } else {
            fastify.storage.updatePreviewInfo(uuid, {
              elmo: null,
              error: 'EMREX_SIGNATURE_VALIDATION_ERROR',
              status: PreviewStatus.Error,
            });
          }
        } catch (err) {
          fastify.storage.updatePreviewInfo(uuid, {
            elmo: null,
            error: 'EMREX_FAILED_TO_DECODE_DATA',
            status: PreviewStatus.Error,
          });

          fastify.log.error({
            id: randomUUID(),
            messageId: null,
            conversationId:
              previewInfo?.data.domibusMessageHeaderInfo?.collaborationInfo
                ?.conversationId,
            operation: 'EMREX_FAILED_TO_DECODE_DATA',
            error: JSON.stringify(err),
          });
        }
      } else {
        fastify.storage.updatePreviewInfo(uuid, {
          elmo: null,
          error: 'EMREX_DATA_XML_VALIDATION_ERROR',
          status: PreviewStatus.Error,
        });
      }

      // Send OOTS respone if second request recieved
      if (previewInfo?.data.secondRequestRecieved) {
        await handlePreviewRequest(fastify, { previewId: uuid });
      }

      previewInfo = fastify.storage.getPreviewInfo(uuid);

      // If there is an error, redirect to BRIDGE_FRONTEND_URL
      if (previewInfo?.data.error !== null) {
        return reply.redirect(
          `${fastify.config.BRIDGE_FRONTEND_URL}?sessionId=${uuid}${
            returnUrl ? `&returnurl=${encodeURIComponent(returnUrl)}` : ''
          }`
        );
      }

      // Update status
      fastify.storage.updatePreviewInfo(uuid, {
        status: PreviewStatus.Finished,
      });

      // Redirect to the return url
      return reply.redirect(returnUrl);
    }
  );
};

export default plugin;
