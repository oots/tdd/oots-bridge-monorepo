import type { RegistryObject } from '@oots-emrex/utils';
import { randomUUID } from 'node:crypto';
import type { UserInfo } from '../../plugins/storage.js';

interface RegistryObjectProps {
  user: UserInfo;
  responseFormat: 'application/xml' | 'application/pdf' | 'application/json';
  attachmentNumber: number;
}

export const createRegistryObject = ({
  user,
  responseFormat,
  attachmentNumber,
}: RegistryObjectProps) => {
  const { surname, dateOfBirth, forenames } = user.query;
  const person = {
    'sdg:NaturalPerson': {
      'sdg:Identifier': {
        _attributes: {
          schemeID: 'eidas',
        },
        _text: 'DK/DE/123456',
      },
      'sdg:FamilyName': {
        _text: surname,
      },
      ...(forenames && {
        'sdg:GivenName': {
          _text: forenames,
        },
      }),
      'sdg:DateOfBirth': {
        _text: dateOfBirth,
      },
    },
  };

  // TODO: THIS IS MOCK DATA
  const registryObject: RegistryObject = {
    _attributes: {
      'xsi:type': 'rim:ExtrinsicObjectType',
      id: `urn:uuid:${randomUUID().toString()}`,
    },
    'rim:Slot': {
      _attributes: {
        name: 'EvidenceMetadata',
      },
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': 'rim:AnyValueType',
        },
        'sdg:Evidence': {
          'sdg:Identifier': {
            _text: randomUUID().toString(),
          },
          'sdg:IsAbout': person,
          'sdg:IssuingAuthority': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
              },
              _text: 'DE73524311',
            },
            'sdg:Name': [
              {
                _attributes: {
                  lang: 'en',
                },
                _text: 'Civil Registration Office Berlin I',
              },
            ],
          },
          'sdg:IsConformantTo': {
            'sdg:EvidenceTypeClassification': {
              _text: `https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DK/${randomUUID().toString()}`,
            },
            'sdg:Title': [
              {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Certificate of Birth',
              },
            ],
            'sdg:Description': [
              {
                _attributes: {
                  lang: 'EN',
                },
                _text:
                  'An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.',
              },
            ],
          },
          'sdg:IssuingDate': {
            _text: '2008-01-01',
          },
          'sdg:Distribution': {
            'sdg:Format': {
              _text: responseFormat,
            },
          },
          'sdg:ValidityPeriod': {
            'sdg:StartDate': {
              _text: '1999-01-01',
            },
            'sdg:EndDate': {
              _text: '2002-01-01',
            },
          },
        },
      },
    },
    'rim:RepositoryItemRef': {
      _attributes: {
        'xlink:href': `cid:${randomUUID()}@example.oots.eu`,
        'xlink:title': `Attachment #${attachmentNumber}`,
      },
    },
  };

  return registryObject;
};
