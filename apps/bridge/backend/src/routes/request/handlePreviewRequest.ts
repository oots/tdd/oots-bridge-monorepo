import { submitMessage, isError } from '@oots-emrex/utils';
import type { FastifyInstance } from 'fastify';
import { PreviewStatus } from '../../plugins/storage.js';
import type { PreviewPayload } from './types.js';
import { createDomibusPayloads } from './createSoapBody.js';
import { randomUUID } from 'node:crypto';
import { createQueryResponseError } from './createQueryResponseError.js';

export const handlePreviewRequest = async (
  fastify: FastifyInstance,
  payload: PreviewPayload
): Promise<{ conversationId?: string }> => {
  const previewInfo = fastify.storage.getPreviewInfo(payload.previewId);

  if (!previewInfo) {
    throw new Error(`Preview info for ${payload.previewId} not found`);
  }

  const status = previewInfo.data.status;

  switch (status) {
    case PreviewStatus.Pending:
      throw new Error('Waiting to receive the second request');
    // Timeout also has ERROR set to TIMEOUT
    case PreviewStatus.Initialized:
    case PreviewStatus.Timeout:
    case PreviewStatus.Error: {
      // Prepare and send Domibus message
      const {
        cid,
        userInfo,
        error,
        elmo,
        domibusMessageHeaderInfo,
        requestId,
        requestedEmrexDataProvider: evidenceProvider,
        evidenceRequester,
        responseFormat,
      } = previewInfo.data;

      if (!domibusMessageHeaderInfo) {
        throw new Error('Missing Domibus message header info');
      }

      if (!cid) {
        throw new Error('Missing CID');
      }

      if (!requestId) {
        throw new Error('Missing requestId');
      }

      let submitMessageResponse;

      const elmConverterUrl = fastify.config.ELM_CONVERTER_URL;

      if (!error) {
        const payloads = await createDomibusPayloads({
          user: userInfo,
          requestId,
          elmo: elmo,
          evidenceProvider,
          evidenceRequester,
          responseFormat,
          elmConverterUrl,
          axiosInstance: fastify.config.AXIOS_INSTANCE,
        });

        // Log payloads
        fastify.log.info({
          id: randomUUID(),
          messageId: null,
          conversationId:
            domibusMessageHeaderInfo.collaborationInfo.conversationId,
          operation: 'PAYLOAD_COUNT',
          error: null,
          data: payloads.length,
        });

        // Send response
        submitMessageResponse = await submitMessage({
          url: fastify.config.DOMIBUS_ACCESS_POINT,
          createHeaderParams: domibusMessageHeaderInfo,
          createBodyParams: {
            payload: {
              parts: payloads.map((item) => ({
                href: item.attributes.payloadId.split(':')[1],
                value: item.value,
                contentType: item.attributes.contentType,
              })),
            },
          },
        });
      } else {
        const errorResponse = createQueryResponseError({
          responseId: randomUUID(),
          requestId,
          evidenceProvider,
          evidenceRequester,
          errorType: error,
        });

        fastify.log.info({
          id: randomUUID(),
          messageId: null,
          conversationId:
            domibusMessageHeaderInfo.collaborationInfo.conversationId,
          operation: error,
          error: null,
          payload: errorResponse,
        });

        const errorResponseBase64 =
          Buffer.from(errorResponse).toString('base64');

        submitMessageResponse = await submitMessage({
          url: fastify.config.DOMIBUS_ACCESS_POINT,
          createHeaderParams: domibusMessageHeaderInfo,
          createBodyParams: {
            payload: {
              parts: [
                {
                  href: `${randomUUID()}@c2.example.com`,
                  value: errorResponseBase64,
                  contentType: 'application/x-ebrs+xml',
                },
              ],
            },
          },
        });
      }

      if (isError(submitMessageResponse)) {
        fastify.log.error({
          id: randomUUID(),
          messageId: null,
          conversationId:
            domibusMessageHeaderInfo.collaborationInfo.conversationId,
          operation: 'SUBMIT_MESSAGE_ERROR',
          error: submitMessageResponse.error,
        });

        throw new Error('An error occurred when sending Domibus response');
      }

      fastify.log.info({
        id: randomUUID(),
        messageId: submitMessageResponse.data.messageId,
        conversationId:
          domibusMessageHeaderInfo.collaborationInfo.conversationId,
        operation: 'MESSAGE_SUBMITTED',
        error: null,
      });

      // Frontend should handle the redirect
      return {
        conversationId:
          domibusMessageHeaderInfo.collaborationInfo.conversationId,
      };
    }
    default:
      console.info(
        `Preview id ${payload.previewId} is in status ${previewInfo?.data.status}`
      );
      return {};
  }
};
