export const transformEvidenceRequester = (evidenceRequestor: any): any => {
  const agents = evidenceRequestor.SlotValue.Element;

  let agent;
  if (Array.isArray(agents)) {
    // Find the one wiht `Classification` = `ER``
    const erAgent = agents.find(
      (agent: any) => agent.Agent.Classification._text === 'ER'
    );

    if (erAgent) {
      agent = erAgent.Agent;
    } else {
      agent = agents[0].Agent;
    }
  } else {
    agent = agents.Agent;
  }

  const transformedAgent = {
    _attributes: {
      name: 'EvidenceRequester',
    },
    'rim:SlotValue': {
      _attributes: {
        'xsi:type': 'rim:AnyValueType',
      },
      'sdg:Agent': {
        'sdg:Identifier': agent.Identifier,
        'sdg:Name': agent.Name,
      },
    },
  };

  return transformedAgent;
};
