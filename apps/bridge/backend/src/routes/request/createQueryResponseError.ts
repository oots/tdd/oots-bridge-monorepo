import type { OOTSQueryResponseError } from '@oots-emrex/utils';
import { js2xml } from 'xml-js';
import { DateTime } from 'luxon';
import { transformEvidenceRequester } from './transformEvidenceRequester.js';

export type ErrorTypes =
  | 'PREVIEW_REQUIRED'
  | 'XML_REQUEST_VALIDATION_ERROR'
  | 'XML_SCHEMATRON_VALIDATION_ERROR'
  | 'STATE_VALIDATION_ERROR'
  | 'EMREX_SIGNATURE_VALIDATION_ERROR'
  | 'EMREX_DATA_PROVIDER_NOT_FOUND'
  | 'EMREX_DATA_XML_VALIDATION_ERROR'
  | 'EMREX_ERROR_RESPONSE'
  | 'EMREX_FAILED_TO_DECODE_DATA'
  | 'IDENTITY_MATCHING_ERROR'
  | 'TIMEOUT'
  | 'UNEXPECTED_ERROR';

interface OOTSQueryResponseErrorProps {
  responseId: string;
  requestId: string;
  previewLocation?: string;
  evidenceRequester: any;
  evidenceProvider: any;
  errorType: ErrorTypes;
}

const ERROR_TYPE_TO_MESSAGE = {
  PREVIEW_REQUIRED: 'Missing Authorization',
  XML_REQUEST_VALIDATION_ERROR: 'XML request validation failed',
  XML_SCHEMATRON_VALIDATION_ERROR: 'XML schematron validation failed',
  STATE_VALIDATION_ERROR: 'State validation failed',
  EMREX_SIGNATURE_VALIDATION_ERROR: 'Signature validation failed',
  EMREX_DATA_PROVIDER_NOT_FOUND: 'EMREX data provider not found',
  EMREX_DATA_XML_VALIDATION_ERROR: 'EMREX data XML validation failed',
  EMREX_ERROR_RESPONSE: 'EMREX error response',
  EMREX_FAILED_TO_DECODE_DATA: 'Failed to decode EMREX data',
  IDENTITY_MATCHING_ERROR: 'Identity matching failed',
  TIMEOUT: 'Timeout',
  UNEXPECTED_ERROR: 'Unexpected error occurred',
};

export const createQueryResponseError = ({
  responseId,
  requestId,
  previewLocation,
  evidenceProvider,
  evidenceRequester,
  errorType,
}: OOTSQueryResponseErrorProps) => {
  let exception: any;

  if (errorType === 'PREVIEW_REQUIRED') {
    exception = {
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'rs:AuthorizationExceptionType',
          severity:
            'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired',
          message: ERROR_TYPE_TO_MESSAGE[errorType],
          detail:
            'The server needs authorisation and preview on its side to process the request',
          code: 'EDM:ERR:0002',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
          {
            _attributes: {
              name: 'PreviewLocation',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:StringValueType',
              },
              'rim:Value': {
                _text: previewLocation,
              },
            },
          },
        ],
      },
    };
  } else if (
    errorType === 'EMREX_ERROR_RESPONSE' ||
    errorType === 'EMREX_DATA_XML_VALIDATION_ERROR' ||
    errorType === 'EMREX_FAILED_TO_DECODE_DATA' ||
    errorType === 'EMREX_SIGNATURE_VALIDATION_ERROR' ||
    errorType === 'UNEXPECTED_ERROR'
  ) {
    exception = {
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'query:QueryExceptionType',
          severity: 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error',
          message: ERROR_TYPE_TO_MESSAGE[errorType],
          code: 'EDM:ERR:0008',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
        ],
      },
    };
  } else if (errorType === 'TIMEOUT') {
    exception = {
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'rs:TimeoutExceptionType',
          severity: 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error',
          message: ERROR_TYPE_TO_MESSAGE[errorType],
          code: 'EDM:ERR:0005',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
        ],
      },
    };
  } else if (
    errorType === 'IDENTITY_MATCHING_ERROR' ||
    errorType === 'EMREX_DATA_PROVIDER_NOT_FOUND'
  ) {
    exception = {
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'rs:AuthorizationExceptionType',
          severity:
            'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:IdentityMatchingError',
          message: ERROR_TYPE_TO_MESSAGE[errorType],
          code: 'EDM:ERR:0002',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
        ],
      },
    };
  } else if (
    errorType === 'XML_REQUEST_VALIDATION_ERROR' ||
    errorType === 'XML_SCHEMATRON_VALIDATION_ERROR' ||
    errorType === 'STATE_VALIDATION_ERROR'
  ) {
    exception = {
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'rs:InvalidRequestExceptionType',
          severity:
            'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:IdentityMatchingError',
          message: ERROR_TYPE_TO_MESSAGE[errorType],
          code: 'EDM:ERR:0003',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
        ],
      },
    };
  }

  const jsonResponse = {
    _declaration: {
      _attributes: {
        version: '1.0',
        encoding: 'utf-8',
      },
    },
    'query:QueryResponse': {
      _attributes: {
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
        'xmlns:sdg': 'http://data.europa.eu/p4s',
        'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
        'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        status: 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure',
        'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
        requestId,
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'SpecificationIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: 'oots-edm:v1.0',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceResponseIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: responseId,
            },
          },
        },
        {
          _attributes: {
            name: 'ErrorProvider',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': evidenceProvider.SlotValue.Agent.Identifier,
              'sdg:Name': evidenceProvider.SlotValue.Agent.Name,
              'sdg:Address': {
                'sdg:FullAddress': {
                  _text: 'Prince Street 42',
                },
                'sdg:LocatorDesignator': {
                  _text: '42',
                },
                'sdg:PostCode': {
                  _text: '1050',
                },
                'sdg:PostCityName': {
                  _text: 'Copenhagen',
                },
                'sdg:AdminUnitLevel1': {
                  _text: 'DK',
                },
                'sdg:AdminUnitLevel2': {
                  _text: 'DK011',
                },
              },
              'sdg:Classification': {
                _text: 'ERRP',
              },
            },
          },
        },
        { ...transformEvidenceRequester(evidenceRequester) },
      ],
      ...exception,
    },
  } as OOTSQueryResponseError;
  const xmlResponse = js2xml(jsonResponse, { compact: true, spaces: 2 });
  return xmlResponse;
};
