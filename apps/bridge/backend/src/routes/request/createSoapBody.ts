import {
  convertElmoToElm,
  type DomibusMessageBodyPayload,
  type OOTSQueryResponse,
} from '@oots-emrex/utils';
import { json2xml, xml2json } from 'xml-js';
import { randomUUID } from 'node:crypto';
import type { UserInfo } from '../../plugins/storage.js';
import { createQueryResponse } from './createQueryResponse.js';
import type { AxiosInstance } from 'axios';

interface CreateDomibusPayloadsProps {
  user: UserInfo;
  requestId: string;
  elmo: string | null;
  evidenceProvider: any;
  evidenceRequester: any;
  responseFormat: 'application/xml' | 'application/pdf' | 'application/json';
  elmConverterUrl: string;
  axiosInstance: AxiosInstance;
}

const getAttachments = (elmoJson: any) => {
  const attachments: any[] = [];

  if (elmoJson.attachment != null) {
    return Array.isArray(elmoJson.attachment)
      ? elmoJson.attachment
      : [elmoJson.attachment];
  }

  if (elmoJson.report?.attachment) {
    return Array.isArray(elmoJson.report.attachment)
      ? elmoJson.report.attachment
      : [elmoJson.report.attachment];
  }

  return attachments;
};

export const createDomibusPayloads = async ({
  user,
  requestId,
  elmo,
  evidenceProvider,
  evidenceRequester,
  responseFormat,
  elmConverterUrl,
  axiosInstance,
}: CreateDomibusPayloadsProps): Promise<DomibusMessageBodyPayload[]> => {
  const payloadArray: DomibusMessageBodyPayload[] = [];

  let queryResponse: OOTSQueryResponse;
  let pdfs: string[] = [];

  if (
    responseFormat === 'application/xml' ||
    responseFormat === 'application/json'
  ) {
    queryResponse = createQueryResponse({
      user,
      requestId,
      evidenceProvider,
      evidenceRequester,
      responseFormat,
      registryObjectCount: 1,
    });
  } else {
    if (elmo) {
      // Extract PDFs from ELMO
      const elmoJson = JSON.parse(
        xml2json(elmo, {
          compact: true,
          spaces: 4,
        })
      );

      const attachments = getAttachments(elmoJson ? elmoJson.elmo : {});

      pdfs = attachments.map((attachment: any) => attachment.content._text);
    }

    queryResponse = createQueryResponse({
      user,
      requestId,
      evidenceProvider,
      evidenceRequester,
      responseFormat,
      registryObjectCount: pdfs.length,
    });
  }

  const xmlPayload = json2xml(JSON.stringify(queryResponse), {
    compact: true,
    spaces: 4,
  });

  const queryResponsePayload: DomibusMessageBodyPayload = {
    attributes: {
      payloadId: `cid:${randomUUID()}@example.oots.eu`,
      contentType: 'application/x-ebrs+xml',
    },
    value: Buffer.from(xmlPayload).toString('base64'),
  };

  const registryObjectList =
    queryResponse['query:QueryResponse']['rim:RegistryObjectList'];

  if (registryObjectList !== undefined) {
    const attachmentIds: string[] = [];

    if (!Array.isArray(registryObjectList['rim:RegistryObject'])) {
      registryObjectList['rim:RegistryObject'] = [
        registryObjectList['rim:RegistryObject'],
      ];
    }

    registryObjectList['rim:RegistryObject'].forEach((registryObject: any) => {
      attachmentIds.push(
        registryObject['rim:RepositoryItemRef']._attributes['xlink:href']
      );
    });

    payloadArray.push(queryResponsePayload);

    if (responseFormat === 'application/xml' && elmo) {
      const elmoPayload: DomibusMessageBodyPayload = {
        attributes: {
          payloadId: attachmentIds[0],
          contentType: 'application/xml',
        },
        value: Buffer.from(elmo).toString('base64'),
      };

      payloadArray.push(elmoPayload);
    } else if (responseFormat === 'application/pdf') {
      pdfs.forEach((pdf, index) => {
        const pdfPayload: DomibusMessageBodyPayload = {
          attributes: {
            payloadId: attachmentIds[index],
            contentType: 'application/pdf',
          },
          value: pdf.replace('data:application/pdf;base64,', ''),
        };

        payloadArray.push(pdfPayload);
      });
    } else {
      if (!elmConverterUrl) {
        throw new Error(
          'Trying to convert ELMO to ELM but the ELM_CONVERTER_URL is not set'
        );
      }

      if (elmo) {
        const elm = await convertElmoToElm(
          elmConverterUrl,
          Buffer.from(elmo).toString('base64'),
          axiosInstance
        );

        const elmPayload: DomibusMessageBodyPayload = {
          attributes: {
            payloadId: attachmentIds[0],
            contentType: 'application/json',
          },
          value: elm,
        };

        payloadArray.push(elmPayload);
      }
    }
  }

  return payloadArray;
};
