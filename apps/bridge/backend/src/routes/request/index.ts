import type { FastifyInstance, FastifyPluginAsync } from 'fastify';
import { AsyncTask, SimpleIntervalJob } from 'toad-scheduler';
import { fastifySchedule } from '@fastify/schedule';
import {
  isError,
  listPendingMessages,
  retrieveMessage,
  submitMessage,
} from '@oots-emrex/utils';
import { randomUUID } from 'node:crypto';
import { PreviewStatus } from '../../plugins/storage.js';
import { createQueryResponseError } from './createQueryResponseError.js';
import { XMLParser } from 'fast-xml-parser';
import { handlePreviewRequest } from './handlePreviewRequest.js';
import { ValidationType } from '../../plugins/xsd-validator.js';
import { readdir } from 'node:fs/promises';

// @ts-ignore There are no types for `saxon-js`
import SaxonJS from 'saxon-js';

const handleRequest = async (
  fastify: FastifyInstance,
  messageId: string
): Promise<void> => {
  // Retrieve message
  const messageResult = await retrieveMessage(
    fastify.config.DOMIBUS_ACCESS_POINT,
    messageId
  );

  // Check for errors
  if (isError(messageResult)) {
    fastify.log.error({
      id: randomUUID(),
      messageId: messageId,
      conversationId: null,
      operation: 'RETRIEVE_MESSAGE_ERROR',
      error: messageResult.error,
    });

    return;
  }

  const { header, body } = messageResult.data;

  const parts = body.payload as any[];

  const response = [];

  let previewLocationSet = false;

  const {
    Messaging: {
      UserMessage: {
        CollaborationInfo: {
          Service: {
            attributes: { type: serviceType },
            $value: serviceValue,
          },
          ConversationId: conversationId,
        },
        PartyInfo: {
          From: {
            PartyId: {
              attributes: { type: fromType },
              $value: fromId,
            },
            Role: fromRole,
          },
          To: {
            PartyId: {
              attributes: { type: toType },
              $value: toId,
            },
            Role: toRole,
          },
        },
      },
    },
  } = header;

  const originalSender =
    header.Messaging.UserMessage.MessageProperties.Property.find(
      (p: any) => p.attributes.name === 'originalSender'
    );

  const finalRecipient =
    header.Messaging.UserMessage.MessageProperties.Property.find(
      (p: any) => p.attributes.name === 'finalRecipient'
    );

  let responseId;

  try {
    for (const part of parts) {
      // Decode base64 payload
      const stringPayload = Buffer.from(part.value, 'base64').toString('utf-8');

      fastify.log.info({
        id: randomUUID(),
        messageId: messageId,
        conversationId: conversationId,
        operation: 'RECEIVE_MESSAGE_PAYLOAD',
        error: null,
        payload: stringPayload,
      });

      // OOTS XML schema validation
      const xmlValidationResult = await fastify.xsdValidator.validateXML(
        stringPayload,
        ValidationType.OOTS,
        (message: string) => {
          fastify.log.info({
            id: randomUUID(),
            messageId: messageId,
            conversationId: conversationId,
            operation: 'REQUEST_XML_VALIDATION_RESULT',
            error: null,
            payload: message,
          });
        }
      );

      if (!xmlValidationResult) {
        throw new Error('XML_REQUEST_VALIDATION_ERROR');
      }

      const files = await readdir('./saxonjs/sef');

      // Parse the validation result using fast-xml-parser
      const parser = new XMLParser({
        ignoreAttributes: false,
        parseAttributeValue: true,
        parseTagValue: true,
      });

      const errors = (
        await Promise.all(
          files.map(async (file) => {
            try {
              const result = await SaxonJS.transform(
                {
                  destination: 'raw',
                  stylesheetLocation: `./saxonjs/sef/${file}`,
                  sourceType: 'xml',
                  sourceText: stringPayload,
                },
                'async'
              );

              const validationResult = parser.parse(result.principalResult);

              // Extract and format the validation information
              const errors =
                validationResult['svrl:schematron-output'][
                  'svrl:failed-assert'
                ];

              return errors ?? [];
            } catch (err: any) {
              console.log(err);
              return [
                {
                  'svrl:text': err.message,
                  '@_role': 'FATAL',
                },
              ];
            }
          })
        )
      )
        .flat()
        .filter((err) => err['@_role'] === 'FATAL');

      const schematronValidationError = errors.length > 0;

      if (schematronValidationError) {
        fastify.log.error({
          id: randomUUID(),
          messageId: messageId,
          conversationId: conversationId,
          operation: 'XML_SCHEMATRON_VALIDATION_ERROR',
          error: JSON.stringify(errors),
        });

        throw new Error('XML_SCHEMATRON_VALIDATION_ERROR');
      }

      const xmlParser = new XMLParser({
        removeNSPrefix: true,
        attributesGroupName: '_attributes',
        ignoreAttributes: false,
        attributeNamePrefix: '',
        alwaysCreateTextNode: true,
        textNodeName: '_text',
        numberParseOptions: {
          hex: true,
          leadingZeros: false,
        },
      });

      const payload = xmlParser.parse(stringPayload);

      const queryRequest = payload.QueryRequest;

      const requestId = queryRequest._attributes.id;

      // Check if previewLocation is present
      const previewLocation = queryRequest.Slot.find(
        (slot: any) => slot._attributes.name === 'PreviewLocation'
      );

      // We need to extract the evidence requester information from the request
      const requester = queryRequest.Slot.find(
        (slot: any) => slot._attributes.name === 'EvidenceRequester'
      );

      if (previewLocation) {
        previewLocationSet = true;

        const previewLocationUrl = previewLocation.SlotValue.Value._text;

        const urlParams = new URLSearchParams(previewLocationUrl.split('?')[1]);
        const previewId = urlParams.get('sessionId');

        if (!previewId) {
          throw new Error('PreviewLocation URL is invalid');
        }

        responseId = previewId;

        // PreviewLocation is present - this is the second request
        const previewInfo = fastify.storage.getPreviewInfo(previewId);

        // Update second request recieved flag
        fastify.storage.updatePreviewInfo(previewId, {
          secondRequestRecieved: true,
        });

        if (!previewInfo) {
          throw new Error('STATE_VALIDATION_ERROR');
        }

        const { userInfo } = previewInfo.data;

        const baseQuery = queryRequest.Query;
        const query = baseQuery.Slot.find(
          (slot: any) => slot._attributes.name === 'NaturalPerson'
        );

        if (query._attributes.name === 'NaturalPerson') {
          const personSlot = query;

          const {
            SlotValue: {
              Person: {
                GivenName: { _text: givenName },
                FamilyName: { _text: familyName },
                DateOfBirth: { _text: dateOfBirth },
              },
            },
          } = personSlot;

          if (userInfo.legalEntityCode !== 'NP') {
            throw new Error('Invalid query');
          }

          if (
            userInfo.query.surname !== familyName ||
            userInfo.query.dateOfBirth !== dateOfBirth ||
            (userInfo.query.forenames && userInfo.query.forenames !== givenName)
          ) {
            throw new Error('Invalid query');
          }
        } else {
          throw new Error('Invalid query');
        }

        const responseFormat = baseQuery.Slot.find(
          (slot: any) => slot._attributes.name === 'EvidenceRequest'
        ).SlotValue.DataServiceEvidenceType.DistributedAs.Format._text;

        if (
          responseFormat !== 'application/pdf' &&
          responseFormat !== 'application/xml' &&
          responseFormat !== 'application/json'
        ) {
          throw new Error(
            'Requested invalid evidence format. Only PDF, XML and JSON are supported.'
          );
        }

        fastify.storage.updatePreviewInfo(previewId, {
          status: PreviewStatus.Initialized,
          domibusMessageHeaderInfo: {
            collaborationInfo: {
              action: 'ExecuteQueryResponse',
              type: serviceType,
              service: serviceValue,
              conversationId: conversationId ?? '',
            },
            fromParty: {
              id: toId,
              type: toType,
              role: toRole,
            },
            toParty: {
              id: fromId,
              type: fromType,
              role: fromRole,
            },
            messageInfo: {
              finalRecipient: {
                value: originalSender?.$value ?? '',
                type: originalSender?.attributes.type ?? '',
              },
              originalSender: {
                value: finalRecipient?.$value ?? '',
                type: finalRecipient?.attributes.type ?? '',
              },
            },
          },
          cid: part.attributes.payloadId.split(':')[1],
          requestId,
          evidenceRequester: requester ?? {},
          responseFormat,
        });

        // Check if ELMO is set then we can send the second response
        if (previewInfo.data.elmo) {
          // Send second response
          await handlePreviewRequest(fastify, { previewId });
        }

        continue;
      }

      // We need to extract the evidence provider information from the request
      const provider = queryRequest.Slot.find(
        (slot: any) => slot._attributes.name === 'EvidenceProvider'
      );

      if (
        !fastify.config.EMREX_DATA_PROVIDERS[
          provider.SlotValue.Agent.Identifier._text
        ]
      ) {
        throw new Error(
          `EMREX_DATA_PROVIDER_NOT_FOUND: Provider ${
            provider.SlotValue.Agent.Identifier._text
          } not found`
        );
      }

      const previewId = randomUUID();
      const currentResponse = {
        payloadId: part.attributes.payloadId,
        responseId: randomUUID(),
        requestId,
        previewLocation: `${fastify.config.BRIDGE_FRONTEND_URL}?sessionId=${previewId}`,
        evidenceRequester: requester || {},
        evidenceProvider: provider || {},
      } as {
        payloadId: string;
        responseId: string;
        requestId: string;
        previewLocation: string;
        evidenceRequester: any;
        evidenceProvider: any;
      };

      const baseQuery = queryRequest.Query;
      const query = baseQuery.Slot.find(
        (slot: any) => slot._attributes.name === 'NaturalPerson'
      );

      if (!query) {
        throw new Error('Invalid query type');
      }

      const personSlot = query;

      const {
        SlotValue: {
          Person: {
            GivenName: { _text: givenName },
            FamilyName: { _text: familyName },
            DateOfBirth: { _text: dateOfBirth },
          },
        },
      } = personSlot;

      // Save to datastore
      fastify.storage.addPreviewInfo(
        previewId,
        {
          legalEntityCode: 'NP',
          query: {
            surname: familyName,
            dateOfBirth,
            forenames: givenName,
          },
        },
        provider
      );

      response.push(currentResponse);
    }

    // Send resposne
    if (response.length === 0) return;

    let actionPrev = 'ExecuteQueryResponse';
    if (!previewLocationSet) {
      actionPrev = 'ExceptionResponse';
    }

    const submitMessageResponse = await submitMessage({
      url: fastify.config.DOMIBUS_ACCESS_POINT,
      createHeaderParams: {
        collaborationInfo: {
          action: actionPrev,
          type: serviceType,
          service: serviceValue,
          conversationId: conversationId ?? '',
        },
        fromParty: {
          id: toId,
          type: toType,
          role: toRole,
        },
        toParty: {
          id: fromId,
          type: fromType,
          role: fromRole,
        },
        messageInfo: {
          finalRecipient: {
            value: originalSender?.$value ?? '',
            type: originalSender?.attributes.type ?? '',
          },
          originalSender: {
            value: finalRecipient?.$value ?? '',
            type: finalRecipient?.attributes.type ?? '',
          },
        },
      },
      createBodyParams: {
        payload: {
          parts: response.map((r) => {
            const errorResponse = createQueryResponseError({
              responseId: r.responseId,
              requestId: r.requestId,
              previewLocation: r.previewLocation,
              evidenceRequester: r.evidenceRequester,
              evidenceProvider: r.evidenceProvider,
              errorType: 'PREVIEW_REQUIRED',
            });

            fastify.log.info({
              id: randomUUID(),
              messageId: messageId,
              conversationId: conversationId,
              operation: 'SUBMIT_MESSAGE_PAYLOAD',
              error: null,
              payload: errorResponse,
            });

            const clgRes = Buffer.from(errorResponse).toString('base64');
            const cid = r.payloadId.split(':')[1];
            return {
              href: cid,
              value: clgRes,
              contentType: 'application/x-ebrs+xml',
            };
          }),
        },
      },
    });

    if (isError(submitMessageResponse)) {
      fastify.log.error({
        id: randomUUID(),
        messageId: messageId,
        conversationId: conversationId,
        operation: 'SUBMIT_MESSAGE_ERROR',
        error: submitMessageResponse.error,
      });

      return;
    }

    fastify.log.info({
      id: randomUUID(),
      messageId: messageId,
      conversationId: conversationId,
      operation: 'SUBMIT_MESSAGE_SUCCESS',
      error: null,
    });
  } catch (err: any) {
    const previewInfo = fastify.storage.getPreviewInfo(responseId!);
    let errorType = 'UNEXPECTED_ERROR';

    if (err.message) {
      if (err.message.includes('XML_REQUEST_VALIDATION_ERROR')) {
        errorType = 'XML_REQUEST_VALIDATION_ERROR';
      } else if (err.message.includes('XML_SCHEMATRON_VALIDATION_ERROR')) {
        errorType = 'XML_SCHEMATRON_VALIDATION_ERROR';
      } else if (err.message.includes('STATE_VALIDATION_ERROR')) {
        errorType = 'STATE_VALIDATION_ERROR';
      } else if (err.message.includes('TIMEOUT')) {
        errorType = 'TIMEOUT';
      } else if (err.message.includes('EMREX_DATA_PROVIDER_NOT_FOUND')) {
        errorType = 'EMREX_DATA_PROVIDER_NOT_FOUND';
      }
    }

    fastify.log.error({
      id: randomUUID(),
      messageId: messageId,
      conversationId: conversationId,
      operation: errorType,
      error: JSON.stringify(err),
    });

    // Update preview info
    fastify.storage.updatePreviewInfo(responseId!, {
      error: errorType as any,
      status: PreviewStatus.Error,
    });

    const errorResponse = createQueryResponseError({
      responseId: randomUUID(),
      requestId: '',
      evidenceProvider: previewInfo?.data.requestedEmrexDataProvider,
      evidenceRequester: {},
      errorType: errorType as any,
    });

    const errorResponseBase64 = Buffer.from(errorResponse).toString('base64');

    const submitMessageResponse = await submitMessage({
      url: fastify.config.DOMIBUS_ACCESS_POINT,
      createHeaderParams: {
        collaborationInfo: {
          action: 'ExceptionResponse',
          type: serviceType,
          service: serviceValue,
          conversationId: conversationId ?? '',
        },
        fromParty: {
          id: toId,
          type: toType,
          role: toRole,
        },
        toParty: {
          id: fromId,
          type: fromType,
          role: fromRole,
        },
        messageInfo: {
          finalRecipient: {
            value: originalSender?.$value ?? '',
            type: originalSender?.attributes.type ?? '',
          },
          originalSender: {
            value: finalRecipient?.$value ?? '',
            type: finalRecipient?.attributes.type ?? '',
          },
        },
      },
      createBodyParams: {
        payload: {
          parts: [
            {
              href: `${randomUUID()}@c2.example.com`,
              value: errorResponseBase64,
              contentType: 'application/x-ebrs+xml',
            },
          ],
        },
      },
    });

    if (isError(submitMessageResponse)) {
      fastify.log.error({
        id: randomUUID(),
        messageId: messageId,
        conversationId: conversationId,
        operation: 'SUBMIT_MESSAGE_ERROR',
        error: submitMessageResponse.error,
      });

      return;
    }

    fastify.log.info({
      id: randomUUID(),
      messageId: messageId,
      conversationId: conversationId,
      operation: 'SUBMIT_MESSAGE_SUCCESS',
      error: null,
    });
  }
};

const plugin: FastifyPluginAsync = async (fastify): Promise<void> => {
  const task = new AsyncTask(
    'Request processing task',
    async () => {
      const listPendingMessagesResult = await listPendingMessages(
        fastify.config.DOMIBUS_ACCESS_POINT
      );

      // Check for errors
      if (isError(listPendingMessagesResult)) {
        fastify.log.error({
          id: randomUUID(),
          messageId: null,
          conversationId: null,
          operation: 'LIST_PENDING_MESSAGES_ERROR',
          error: listPendingMessagesResult.error,
        });

        return;
      }

      const { messageIds } = listPendingMessagesResult.data;

      await Promise.all(
        messageIds.map(async (messageId) => {
          fastify.log.info({
            id: randomUUID(),
            messageId: messageId,
            conversationId: null,
            operation: 'PROCESSING_MESSAGE',
            error: null,
          });

          try {
            await handleRequest(fastify, messageId);
            fastify.log.info({
              id: randomUUID(),
              messageId: messageId,
              conversationId: null,
              operation: 'MESSAGE_PROCESSED',
              error: null,
            });
          } catch (err) {
            fastify.log.error({
              id: randomUUID(),
              messageId: messageId,
              conversationId: null,
              operation: 'UNEXPECTED_ERROR',
              error: JSON.stringify(err),
            });
          }
        })
      );
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );

  // Create the job
  const job = new SimpleIntervalJob({ seconds: 1 }, task);

  // Register the scheduler
  await fastify.register(fastifySchedule);

  // Start the scheduler
  fastify.ready().then(
    () => {
      fastify.scheduler.addSimpleIntervalJob(job);
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );
};

export default plugin;
