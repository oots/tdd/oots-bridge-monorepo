import type { OOTSQueryResponse } from '@oots-emrex/utils';
import { createRegistryObject } from './createRegistryObject.js';
import type { UserInfo } from '../../plugins/storage.js';
import { transformEvidenceRequester } from './transformEvidenceRequester.js';

interface OOTSQueryResponseProps {
  user: UserInfo;
  requestId: string;
  elmo?: string;
  evidenceRequester: any;
  evidenceProvider?: any;
  responseFormat: 'application/xml' | 'application/pdf' | 'application/json';
  registryObjectCount: number;
}

export const createQueryResponse = ({
  user,
  requestId,
  evidenceProvider,
  evidenceRequester,
  responseFormat,
  registryObjectCount,
}: OOTSQueryResponseProps): OOTSQueryResponse => {
  const queryResponse: OOTSQueryResponse = {
    'query:QueryResponse': {
      _attributes: {
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
        'xmlns:sdg': 'http://data.europa.eu/p4s',
        'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
        'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        status: 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success',
        'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
        requestId,
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'SpecificationIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: 'oots-edm:v1.0',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceResponseIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: '5af62cce-debe-11ec-9d64-0242ac120002', // ID Of the request message
            },
          },
        },
        {
          _attributes: {
            name: 'IssueDateTime',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:DateTimeValueType',
            },
            'rim:Value': {
              _text: '2022-05-19T17:10:10.872Z',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceProvider',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:CollectionValueType',
            },
            'rim:Element': {
              _attributes: {
                'xsi:type': 'rim:AnyValueType',
              },
              'sdg:Agent': [
                {
                  'sdg:Identifier': evidenceProvider.SlotValue.Agent.Identifier,
                  'sdg:Name': evidenceProvider.SlotValue.Agent.Name,
                  'sdg:Classification': {
                    _text: 'EP',
                  },
                },
              ],
            },
          },
        },
        { ...transformEvidenceRequester(evidenceRequester) },
        {
          _attributes: {
            name: 'ResponseAvailableDateTime',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:DateTimeValueType',
            },
            'rim:Value': {
              _text: '2022-05-30T15:00:00.000Z',
            },
          },
        },
      ],
      'rim:RegistryObjectList': {
        'rim:RegistryObject': Array.from({ length: registryObjectCount }).map(
          (_, index) =>
            createRegistryObject({
              user,
              attachmentNumber: index + 1,
              responseFormat,
            })
        ),
      },
    },
  };

  return queryResponse;
};
