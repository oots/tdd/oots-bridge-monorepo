import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  fastify.get('/', async (_, reply) => {
    return reply.code(200).send(fastify.config.EMREX_DATA_PROVIDERS);
  });
};

export default plugin;
