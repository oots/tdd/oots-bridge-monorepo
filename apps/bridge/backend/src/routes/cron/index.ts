import fastifySchedule from '@fastify/schedule';
import type { FastifyPluginAsync } from 'fastify';
import { randomUUID } from 'node:crypto';
import { AsyncTask, CronJob } from 'toad-scheduler';

const plugin: FastifyPluginAsync = async (fastify): Promise<void> => {
  // Create the timeout check task
  const timeoutCheckTask = new AsyncTask(
    'Storage timeout check task',
    async () => {
      fastify.log.info({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'STORAGE_TIMEOUT_CHECK',
        error: null,
      });

      await fastify.storage.checkTimeouts(fastify);
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );

  // Create timeout check job
  const timeoutCheckJob = new CronJob(
    {
      // Runs every 5 minutes
      cronExpression: '*/5 * * * *',
    },
    timeoutCheckTask,
    {
      preventOverrun: true,
    }
  );

  // Register the scheduler
  await fastify.register(fastifySchedule);

  // Start the scheduler
  fastify.ready().then(
    () => {
      // fastify.scheduler.addCronJob(cleanupJob);
      fastify.scheduler.addCronJob(timeoutCheckJob);
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );
};

export default plugin;
