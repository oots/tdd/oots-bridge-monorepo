import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';
import { randomUUID } from 'node:crypto';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  fastify.post(
    '/',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            previewId: { type: 'string' },
            returnUrl: { type: 'string' },
          },
          required: ['previewId', 'returnUrl'],
        },
      },
    },
    async (request, reply) => {
      const { previewId, returnUrl } = request.body;

      const previewInfo = fastify.storage.getPreviewInfo(previewId);

      fastify.storage.setReturnUrl(previewId, returnUrl);

      fastify.log.info({
        id: randomUUID(),
        messageId: null,
        conversationId:
          previewInfo?.data.domibusMessageHeaderInfo?.collaborationInfo
            .conversationId,
        operation: 'PREVIEW_URL_VISITED',
        error: null,
      });

      return reply.code(204).send();
    }
  );
};

export default plugin;
