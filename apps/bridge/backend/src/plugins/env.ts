import envSchema, { type JSONSchemaType } from 'env-schema';
import fp from 'fastify-plugin';
import axios, { type AxiosInstance } from 'axios';
import { HttpsProxyAgent } from 'https-proxy-agent';
export interface Env {
  // Domibus access point
  DOMIBUS_ACCESS_POINT: string;

  // Bridge party information
  PARTY_ID: string;

  // Bridge frontend URL
  BRIDGE_FRONTEND_URL: string;

  // Emrex data providers
  EMREX_DATA_PROVIDERS: string;

  // Forward proxy URL
  PROXY_URL: string;

  // ELM Converter URL
  ELM_CONVERTER_URL: string;

  // Timeout in seconds
  TIMEOUT_IN_SECONDS: number;

  // Levenshtein threshold
  LEVENSHTEIN_THRESHOLD: number;
}

declare module 'fastify' {
  export interface FastifyInstance {
    config: {
      DOMIBUS_ACCESS_POINT: string;
      PARTY_ID: string;
      BRIDGE_FRONTEND_URL: string;
      EMREX_DATA_PROVIDERS: { [key: string]: string };
      EMREX_DATA_PROVIDERS_CERTIFICATES: { [key: string]: string };
      AXIOS_INSTANCE: AxiosInstance;
      ELM_CONVERTER_URL: string;
      TIMEOUT_IN_SECONDS: number;
      LEVENSHTEIN_THRESHOLD: number;
    };
  }
}

/**
 * This plugins adds some utilities to handle loading ENV variables
 *
 * @see https://github.com/fastify/fastify-env
 */
export default fp(async (fastify, _) => {
  const schema: JSONSchemaType<Env> = {
    type: 'object',
    properties: {
      DOMIBUS_ACCESS_POINT: {
        type: 'string',
      },
      PARTY_ID: {
        type: 'string',
      },
      BRIDGE_FRONTEND_URL: {
        type: 'string',
      },
      EMREX_DATA_PROVIDERS: {
        type: 'string',
      },
      PROXY_URL: {
        type: 'string',
      },
      ELM_CONVERTER_URL: {
        type: 'string',
      },
      TIMEOUT_IN_SECONDS: {
        type: 'number',
        default: 3600,
      },
      LEVENSHTEIN_THRESHOLD: {
        type: 'number',
        default: 0,
      },
    },
    required: ['DOMIBUS_ACCESS_POINT', 'PARTY_ID', 'EMREX_DATA_PROVIDERS'],
  };

  const envConfig = envSchema({
    schema,
    dotenv: true,
  });

  const proxy = envConfig.PROXY_URL
    ? new HttpsProxyAgent(envConfig.PROXY_URL)
    : undefined;

  const axiosInstance = axios.create({
    httpsAgent: proxy,
    httpAgent: proxy,
  });

  const emrexDataProviders = JSON.parse(envConfig.EMREX_DATA_PROVIDERS) as {
    [key: string]: {
      url: string;
      certificate: string;
    };
  };

  const dataProviders = new Map<string, string>();
  const dataProvidersCertificates = new Map<string, string>();

  for (const [key, value] of Object.entries(emrexDataProviders)) {
    dataProviders.set(key, value.url);
    dataProvidersCertificates.set(key, value.certificate);
  }

  const config = {
    DOMIBUS_ACCESS_POINT: envConfig.DOMIBUS_ACCESS_POINT,
    PARTY_ID: envConfig.PARTY_ID,
    BRIDGE_FRONTEND_URL: envConfig.BRIDGE_FRONTEND_URL,
    EMREX_DATA_PROVIDERS: Object.fromEntries(dataProviders),
    EMREX_DATA_PROVIDERS_CERTIFICATES: Object.fromEntries(
      dataProvidersCertificates
    ),
    AXIOS_INSTANCE: axiosInstance,
    ELM_CONVERTER_URL: envConfig.ELM_CONVERTER_URL,
    TIMEOUT_IN_SECONDS: envConfig.TIMEOUT_IN_SECONDS,
    LEVENSHTEIN_THRESHOLD: envConfig.LEVENSHTEIN_THRESHOLD,
  };

  fastify.decorate('config', config);
});
