import type { CreateHeaderParams } from '@oots-emrex/utils';
import type { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import type { ErrorTypes } from '../routes/request/createQueryResponseError.js';
import { DateTime } from 'luxon';
import { randomUUID } from 'node:crypto';
import { handlePreviewRequest } from '../routes/request/handlePreviewRequest.js';
import type { FlatCache } from 'flat-cache';

interface StoredObject<T> {
  created: number;
  data: T;
}

export enum PreviewStatus {
  Pending = 0,
  Initialized = 1,
  Finished = 2,
  Error = 3,
  Timeout = 4,
}

interface PreviewInfo {
  // General info
  status: PreviewStatus;
  secondRequestRecieved: boolean;
  error: ErrorTypes | null;
  // User info
  userInfo: UserInfo;
  // Domibus message info
  domibusMessageHeaderInfo?: CreateHeaderParams;
  // Elmo
  requestedEmrexDataProvider: any;
  elmo: string | null;
  // OOTS Message info
  evidenceRequester: any;
  responseFormat: 'application/xml' | 'application/pdf' | 'application/json';
  cid?: string;
  returnUrl: string | null;
  requestId: string | null;
}

export type UserInfo = {
  legalEntityCode: 'NP';
} & {
  legalEntityCode: 'NP';
  query: {
    surname: string;
    dateOfBirth: string;
    forenames?: string;
  };
};

type PreviewInfoObject = StoredObject<PreviewInfo>;
type ReturnUrlObject = StoredObject<{
  returnUrl: string;
}>;

class StorageService {
  cache: FlatCache;

  constructor(cache: FlatCache) {
    this.cache = cache;
  }

  addPreviewInfo(
    previewId: string,
    userInfo: UserInfo,
    emrexDataProvider: string
  ): void {
    this.cache.set(`preview_${previewId}`, {
      created: DateTime.now().toUnixInteger(),
      data: {
        status: PreviewStatus.Pending,
        secondRequestRecieved: false,
        userInfo,
        error: null,
        requestedEmrexDataProvider: emrexDataProvider,
        elmo: null,
        returnUrl: null,
        requestId: null,
        evidenceRequester: null,
        responseFormat: 'application/xml',
      },
    });
  }

  updatePreviewInfo(previewId: string, data: Partial<PreviewInfo>): void {
    const stored = this.cache.get<PreviewInfoObject>(`preview_${previewId}`);

    if (!stored) {
      return;
    }

    stored.data = {
      ...stored.data,
      ...data,
    };

    this.cache.set(`preview_${previewId}`, stored);
  }

  getPreviewInfo(previewId: string): PreviewInfoObject | null {
    const stored = this.cache.get<PreviewInfoObject>(`preview_${previewId}`);

    if (!stored) return null;
    return structuredClone(stored);
  }

  deletePreviewInfo(previewId: string): void {
    this.cache.delete(`preview_${previewId}`);
  }

  setReturnUrl(previewId: string, returnUrl: string): void {
    this.cache.set(`returnUrl_${previewId}`, {
      created: Date.now(),
      data: {
        returnUrl,
      },
    });
  }

  getReturnUrl(previewId: string): string | null {
    const stored = this.cache.get<ReturnUrlObject>(`returnUrl_${previewId}`);

    if (!stored) return null;
    return stored.data.returnUrl;
  }

  async checkTimeouts(fastify: FastifyInstance): Promise<void> {
    const now = DateTime.now();

    for (const { key, value } of this.cache.items) {
      const inputDate = DateTime.fromMillis(value.created);
      const { error, status } = value.data;
      const previewId = key.split('_')[1];

      // Request older than X seconds is considered timed out
      if (
        error === null &&
        status === PreviewStatus.Initialized &&
        now.diff(inputDate, 'seconds').seconds >
          fastify.config.TIMEOUT_IN_SECONDS
      ) {
        fastify.log.info({
          id: randomUUID(),
          messageId: null,
          conversationId: previewId,
          operation: 'TIMEOUT',
          error: null,
        });

        this.updatePreviewInfo(previewId, {
          status: PreviewStatus.Timeout,
          error: 'TIMEOUT',
        });

        // Send response
        await handlePreviewRequest(fastify, {
          previewId: previewId,
        });
      }
    }
  }
}

export default fp<FastifyPluginOptions>(async (fastify, _opts) => {
  const storage = new StorageService(fastify.cache);

  fastify.decorate('storage', storage);
});

declare module 'fastify' {
  export interface FastifyInstance {
    storage: StorageService;
  }
}
