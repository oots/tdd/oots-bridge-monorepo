import type { FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { readdir, readFile } from 'node:fs/promises';
import { resolve } from 'node:path';
import { validateXML, memoryPages } from 'xmllint-wasm';

export enum ValidationType {
  EMREX = 'EMREX',
  OOTS = 'OOTS',
}

type XSDSchemaType = {
  fileName: string;
  contents: string;
};

// Recursively read all files in a directory
const readDirRecursive = async (dir: string) => {
  const dirents = await readdir(dir, { withFileTypes: true });
  const files = await Promise.all(
    dirents.map(async (dirent): Promise<XSDSchemaType[]> => {
      const res = resolve(dir, dirent.name);
      return dirent.isDirectory()
        ? readDirRecursive(res)
        : [{ contents: await readFile(res, 'utf8'), fileName: dirent.name }];
    })
  );
  return files.flat();
};

class XSDValidatorService {
  emrexSchemas: XSDSchemaType[] = [];
  emrexMainSchemas: XSDSchemaType[] = [];
  ootsSchemas: XSDSchemaType[] = [];
  ootsMainSchemas: XSDSchemaType[] = [];

  // Read schemas and store them in memory
  async init() {
    const _emrexSchemas = await readDirRecursive('./src/xsd-schemas/emrex');
    const _ootsSchemas = await readDirRecursive('./src/xsd-schemas/oots');

    this.emrexSchemas = _emrexSchemas.filter(
      (file) => file.fileName !== 'elmo.xsd'
    );
    this.ootsSchemas = _ootsSchemas.filter(
      (file) => file.fileName !== 'oots_driver_v1.0.0.xsd'
    );

    this.emrexMainSchemas = [
      _emrexSchemas.find((file) => file.fileName === 'elmo.xsd')!,
    ];
    this.ootsMainSchemas = [
      _ootsSchemas.find((file) => file.fileName === 'oots_driver_v1.0.0.xsd')!,
    ];
  }

  async validateXML(
    xml: string,
    validationType: ValidationType,
    log: (message: string) => void
  ): Promise<boolean> {
    try {
      const validationResult = await validateXML({
        xml: xml,
        schema:
          validationType === ValidationType.EMREX
            ? this.emrexMainSchemas
            : this.ootsMainSchemas,
        preload:
          validationType === ValidationType.EMREX
            ? this.emrexSchemas
            : this.ootsSchemas,
        maxMemoryPages: memoryPages.GiB,
      });

      log(JSON.stringify(validationResult));
      return validationResult.valid;
    } catch (e) {
      return false;
    }
  }
}

export default fp<FastifyPluginOptions>(async (fastify, _opts) => {
  const xsdValidator = new XSDValidatorService();
  await xsdValidator.init();

  fastify.decorate('xsdValidator', xsdValidator);
});

declare module 'fastify' {
  export interface FastifyInstance {
    xsdValidator: XSDValidatorService;
  }
}
