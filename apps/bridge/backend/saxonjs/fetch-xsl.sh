#!/bin/bash

# Default values
GITLAB_URL=${GITLAB_URL:-"https://code.europa.eu/oots/tdd/tdd_chapters"}
BRANCH=${BRANCH:-"master"}
OUTPUT_DIR=${OUTPUT_DIR:-"$(pwd)/saxonjs/xsl"}

# Help function
show_help() {
    echo "Usage: $0 [options]"
    echo "Options:"
    echo "  -u, --url        GitLab URL (default: https://gitlab.com)"
    echo "  -p, --project    Project ID or repository URL (e.g., 'username/project' or full URL)"
    echo "  -b, --branch     Branch name (default: main)"
    echo "  -o, --output     Output directory (default: downloaded_files)"
    echo "  -t, --token      GitLab API token (optional, required for private repositories)"
    echo "  -h, --help       Show this help message"
    echo ""
    echo "Environment variables:"
    echo "  GITLAB_FILES     Comma-separated list of files to download"
    echo "  Example: GITLAB_FILES=\"README.md,src/main.js,config/settings.json\""
    echo ""
    echo "Example usage:"
    echo "  GITLAB_FILES=\"README.md,LICENSE\" $0 -p gitlab-org/gitlab-foss"
    echo "  GITLAB_FILES=\"src/main.js,package.json\" $0 -p https://gitlab.com/username/project"
}

# Function to extract project path from URL
extract_project_path() {
    local url="$1"
    # Remove protocol and domain if present
    local path=$(echo "$url" | sed -E 's#^(https?://[^/]+/)?##')
    # Remove any trailing .git
    path=$(echo "$path" | sed 's/\.git$//')
    # Remove any trailing slashes
    path=$(echo "$path" | sed 's#/*$##')
    # Remove any GitLab-specific suffixes (like /-/tree/main)
    path=$(echo "$path" | sed 's#/-/.*##')
    echo "$path"
}

# Function to get project ID from path
get_project_id() {
    local project_path="$1"
    local encoded_path=$(echo "$project_path" | sed 's|/|%2F|g')

    echo "$GITLAB_URL/api/v4/projects/$encoded_path"

    # Try to get project ID using API without token
    local response=$(curl --silent --fail "$GITLAB_URL/api/v4/projects/$encoded_path")

    if [[ $? -eq 0 ]]; then
        # Extract ID from JSON response
        echo "$response" | grep -o '"id":[0-9]*' | sed 's/"id"://'
        return 0
    else
        # If the first attempt failed and we have a token, try with token
        if [[ -n "$GITLAB_TOKEN" ]]; then
            response=$(curl --silent --fail --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
                "$GITLAB_URL/api/v4/projects/$encoded_path")
            if [[ $? -eq 0 ]]; then
                echo "$response" | grep -o '"id":[0-9]*' | sed 's/"id"://'
                return 0
            fi
        fi
    fi
    return 1
}

# Function to URL encode strings
urlencode() {
    local string="$1"
    echo "$string" | perl -MURI::Escape -ne 'chomp;print uri_escape($_)'
}

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        -u|--url)
            GITLAB_URL="$2"
            shift 2
            ;;
        -p|--project)
            PROJECT_INPUT="$2"
            shift 2
            ;;
        -b|--branch)
            BRANCH="$2"
            shift 2
            ;;
        -o|--output)
            OUTPUT_DIR="$2"
            shift 2
            ;;
        -t|--token)
            GITLAB_TOKEN="$2"
            shift 2
            ;;
        -h|--help)
            show_help
            exit 0
            ;;
        *)
            echo "Unknown option: $1"
            show_help
            exit 1
            ;;
    esac
done

# Check if project input is provided
if [[ -z "$PROJECT_INPUT" ]]; then
    echo "Error: Project ID or URL is required"
    exit 1
fi

# Check if GITLAB_FILES environment variable is set
if [[ -z "$GITLAB_FILES" ]]; then
    echo "Error: GITLAB_FILES environment variable is not set"
    echo "Please set GITLAB_FILES with a comma-separated list of files to download"
    echo "Example: GITLAB_FILES=\"README.md,src/main.js\" $0 ..."
    exit 1
fi

# Determine if input is numeric (project ID) or needs to be resolved
if [[ "$PROJECT_INPUT" =~ ^[0-9]+$ ]]; then
    GITLAB_PROJECT_ID="$PROJECT_INPUT"
else
    # Extract project path and get project ID
    PROJECT_PATH=$(extract_project_path "$PROJECT_INPUT")
    echo "Resolving project ID for: $PROJECT_PATH"

    GITLAB_PROJECT_ID=$(get_project_id "$PROJECT_PATH")
    if [[ $? -ne 0 || -z "$GITLAB_PROJECT_ID" ]]; then
        echo "Error: Could not resolve project ID. Please check the repository URL/path or provide the project ID directly."
        exit 1
    fi
    echo "Resolved project ID: $GITLAB_PROJECT_ID"
fi

# Create output directory if it doesn't exist
mkdir -p "$OUTPUT_DIR"

# Process comma-separated list of files
IFS=',' read -ra FILES <<< "$GITLAB_FILES"
for file in "${FILES[@]}"; do
    # Trim whitespace
    file=$(echo "$file" | sed 's/^[[:space:]]*//;s/[[:space:]]*$//')
    file_name=$(echo "$file" | sed 's|^.*/||')

    # Skip if empty
    [[ -z "$file" ]] && continue

    # Create the directory structure for the file
    mkdir -p "$OUTPUT_DIR/$(dirname "$file_name")"

    echo "Downloading: $file"

    # Prepare curl command
    curl_cmd="curl --silent --fail"

    # Add token header if provided
    if [[ -n "$GITLAB_TOKEN" ]]; then
        curl_cmd="$curl_cmd --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\""
    fi

    # Add URL and output file
    curl_cmd="$curl_cmd \"$GITLAB_URL/-/raw/$BRANCH/$file\" --output \"$OUTPUT_DIR/$file_name\""

    eval $curl_cmd

    if [[ $? -eq 0 ]]; then
        echo "Successfully downloaded: $file"
    else
        echo "Failed to download: $file"
        echo "If this is a private repository, make sure to provide a valid GitLab token"
    fi
done

echo "Download process completed!"
