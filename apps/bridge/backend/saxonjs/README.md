### Example usage of `fetch-xsl.sh` script

NOTE: Run the script from the root of the repository

```bash
GITLAB_FILES="OOTS-EDM/xslt/EDM-REQ-C.xsl,OOTS-EDM/xslt/EDM-REQ-S.xsl" ./saxonjs/fetch-xsl.sh -p https://code.europa.eu/oots/tdd/tdd_chapters -b 1.0.5
````
