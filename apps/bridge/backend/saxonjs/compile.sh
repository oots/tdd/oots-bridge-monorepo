#!/bin/bash

# Configuration
XSL_DIR="$(pwd)/saxonjs/xsl"
OUTPUT_DIR="$(pwd)/saxonjs/sef"

# Check if XSL directory exists
if [ ! -d "$XSL_DIR" ]; then
    echo "Error: XSL directory '$XSL_DIR' not found!"
    exit 1
fi

# Create output directory if it doesn't exist
mkdir -p "$OUTPUT_DIR"

# Counter for processed files
processed=0
errors=0

# Process each XSL file
for xsl_file in "$XSL_DIR"/*.xsl; do
    if [ -f "$xsl_file" ]; then
        # Extract filename without path and extension
        filename=$(basename "$xsl_file" .xsl)

        echo "Processing: $filename.xsl"

        # Run the transformation command
        if pnpx xslt3 -t \
            -xsl:"$xsl_file" \
            -export:"$OUTPUT_DIR/$filename.sef.json" \
            -nogo \
            -relocate:on \
            -ns:##html5; then

            echo "✓ Successfully processed $filename.xsl"
            ((processed++))
        else
            echo "✗ Error processing $filename.xsl"
            ((errors++))
        fi
        echo "----------------------------------------"
    fi
done

# Summary
echo ""
echo "Processing complete!"
echo "Files processed successfully: $processed"
echo "Files with errors: $errors"
echo "Output files are located in: $OUTPUT_DIR"

# Exit with error if any transformations failed
if [ $errors -gt 0 ]; then
    exit 1
fi
