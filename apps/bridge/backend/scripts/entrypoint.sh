#!/bin/sh

# Fetch environment variables or set defaults
: "${GITLAB_FILES:=OOTS-EDM/xslt/EDM-REQ-C.xsl,OOTS-EDM/xslt/EDM-REQ-S.xsl}"
: "${BASE_URL:=https://code.europa.eu/oots/tdd/tdd_chapters}"
: "${BRANCH:=1.0.5}"

# First fetch XSL files from GitLab
GITLAB_FILES="$GITLAB_FILES" ./saxonjs/fetch-xsl.sh -p "$BASE_URL" -b "$BRANCH"

# Then run the compile script
./saxonjs/compile.sh

exec "$@"
