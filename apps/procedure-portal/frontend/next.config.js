const path = require('node:path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath: process.env.NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL || '',
  env: {
    NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL:
      process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL ||
      'NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL_PLACEHOLDER',
    NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL:
      process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL ||
      'NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL_PLACEHOLDER',
  },
  reactStrictMode: true,
  output: process.env.DOCKER_BUILD === 'true' ? 'standalone' : undefined,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '**',
      },
    ],
    loader: 'default',
  },
  optimizeFonts: true,
  experimental: {
    outputFileTracingRoot: path.join(__dirname, '../../../'),
    outputFileTracingExcludes: {
      '*': [
        'node_modules/@swc/core-linux-x64-gnu',
        'node_modules/@swc/core-linux-x64-musl',
        'node_modules/@esbuild/linux-x64',
      ],
    },
  },
  eslint: {
    ignoreDuringBuilds: true,
  },

  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });
    return config;
  },
};

module.exports = nextConfig;
