#!/bin/sh

find /app/apps/procedure-portal/frontend/.next \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i "s#NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL_PLACEHOLDER#$NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL#g"
find /app/apps/procedure-portal/frontend/.next \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i "s#NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL_PLACEHOLDER#$NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL#g"

exec "$@"
