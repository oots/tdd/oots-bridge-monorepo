import { Home } from '@/components/Home';
import { Spinner } from '@nextui-org/react';
import { Suspense } from 'react';

export const dynamic = 'force-dynamic';

export default async function Page() {
  const emrexDataProviders = await fetch(
    `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL}/emrex-data-providers`,
    {
      cache: 'no-cache',
    }
  ).then((res) => res.json());

  const emrexDataProviderOptions = Object.entries(emrexDataProviders).map(
    ([key, value]) => ({
      label: value as string,
      value: key as string,
    })
  );

  return (
    <main className="min-h-screen h-screen flex justify-center items-center">
      <div className="w-full max-w-xl">
        <h1 className="text-3xl font-bold text-center mb-4">
          OOTS Procedure portal
        </h1>
        <Suspense
          fallback={
            <div className="flex justify-center">
              <Spinner />
            </div>
          }
        >
          <Home emrexDataProviderOptions={emrexDataProviderOptions} />
        </Suspense>
      </div>
    </main>
  );
}
