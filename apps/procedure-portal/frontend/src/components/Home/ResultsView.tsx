import { selectConversationId, useGeneralStore } from '@/stores';
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
} from '@nextui-org/react';
import { useQuery } from '@tanstack/react-query';
import { useCallback, useMemo, useRef } from 'react';

const COLUMNS = [
  { key: 'name', label: 'Name' },
  { key: 'type', label: 'Type' },
  { key: 'size', label: 'Size' },
  { key: 'download', label: 'Download' },
] as const;

export const ResultsView = () => {
  const formRef = useRef<HTMLFormElement | null>(null);

  const conversationId = useGeneralStore(selectConversationId);

  const { data } = useQuery<{
    previewLocation: string;
    elmo?: any;
    responseNumber: number;
    emrexSession?: {
      sessionId: string;
      returnUrl: string;
    };
    elmoXmlBase64?: string;
  }>({
    queryKey: ['messageData', conversationId],
  });

  const attachments: any[] = useMemo(() => {
    const elmo = data?.elmo;

    if (elmo) {
      if (elmo.attachment != null) {
        return Array.isArray(elmo.attachment)
          ? elmo.attachment
          : [elmo.attachment];
      }

      if (elmo.report?.attachment) {
        return Array.isArray(elmo.report.attachment)
          ? elmo.report.attachment
          : [elmo.report.attachment];
      }
    }

    return [];
  }, [data]);

  const renderCell = useCallback((item: any, columnKey: any) => {
    switch (columnKey) {
      case 'size':
        return `${(item.content._text.length / (1024 * 1024)).toPrecision(
          2
        )} MB`;
      case 'download':
        return (
          <a
            href={item.content._text}
            download={`emrex-data-${new Date().getTime()}.pdf`}
          >
            Download PDF
          </a>
        );
      case 'type':
        return item.type._text;
      case 'name': {
        const titles = Array.isArray(item.title) ? item.title : [item.title];

        const title = titles.find(
          (t: any) => t._attributes['xml:lang'] === 'en'
        );

        return title?._text || titles[0]._text;
      }

      default:
        return '';
    }
  }, []);

  return (
    <div>
      {attachments.length === 0 && (
        <div className="flex justify-center mb-4">
          <p className="text-center max-w-md">
            The evidence transfer request has been either declined or no
            evidence was found
          </p>
        </div>
      )}
      {attachments.length > 0 && (
        <Table className="mb-4">
          <TableHeader>
            {COLUMNS.map((column) => (
              <TableColumn key={column.key}>{column.label}</TableColumn>
            ))}
          </TableHeader>
          <TableBody>
            {attachments.map((row, index) => (
              <TableRow key={index}>
                {(columnKey) => (
                  <TableCell>{renderCell(row, columnKey)}</TableCell>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      )}
      {attachments.length > 0 && data?.emrexSession && (
        <div className="flex justify-center items-center">
          <div className="hidden">
            <form
              ref={formRef}
              action={data.emrexSession.returnUrl}
              method="POST"
            >
              <input
                type="hidden"
                name="sessionId"
                value={data.emrexSession.sessionId}
              />
              <input type="hidden" name="returnCode" value="NCP_OK" />
              <input
                type="hidden"
                name="elmo"
                value={data.elmoXmlBase64 ?? ''}
              />
            </form>
          </div>
          <div className="flex flex-col gap-2">
            <div>Share data with the following emrex client:</div>
            <div>{data.emrexSession.returnUrl}</div>
          </div>
          <Button onClick={() => formRef.current?.submit()}>Share</Button>
        </div>
      )}
      <div className="flex justify-center mt-4">
        <Button
          onClick={() =>
            window.location.assign(
              `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL}`
            )
          }
        >
          Reset form
        </Button>
      </div>
    </div>
  );
};
