import {
  selectConversationId,
  selectPreviewLocation,
  useGeneralStore,
} from '@/stores';
import { Button } from '@nextui-org/react';
import { useCallback } from 'react';

export const SecondRequestView = () => {
  const conversationId = useGeneralStore(selectConversationId);
  const previewLocation = useGeneralStore(selectPreviewLocation);

  const sendSecondRequest = useCallback(async () => {
    await fetch(
      `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL}/message/${conversationId}`,
      {
        method: 'POST',
      }
    );
  }, [conversationId]);

  return (
    <div className="w-full flex space-x-4">
      <Button fullWidth variant="shadow" color="primary">
        <a
          href={`${previewLocation}&returnurl=${encodeURIComponent(
            `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL}/?conversationId=${conversationId}`
          )}`}
          target="_blank"
          rel="noreferrer"
        >
          Go to preview space
        </a>
      </Button>
      <Button
        fullWidth
        variant="shadow"
        color="primary"
        onClick={sendSecondRequest}
      >
        Send second request
      </Button>
    </div>
  );
};
