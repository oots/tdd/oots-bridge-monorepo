import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Button, Input, Select, SelectItem } from '@nextui-org/react';
import { selectEmrexSessionId, useGeneralStore } from '@/stores';
import { DateTime } from 'luxon';

const schema = yup
  .object({
    name: yup.string().required(),
    surname: yup.string().required(),
    dateOfBirth: yup
      .date()
      .max(new Date(), 'You cannot be born in the future!')
      .required(),
    provider: yup.string().required(),
  })
  .required();

type FormData = {
  name: string;
  surname: string;
  dateOfBirth: Date;
  provider: string;
};

export const UserFormView = ({
  emrexDataProviderOptions,
}: { emrexDataProviderOptions: { label: string; value: string }[] }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
  });

  const emrexSessionId = useGeneralStore(selectEmrexSessionId);

  const onSubmit = async (data: FormData) => {
    const result = await fetch(
      `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL!}/message`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          ...data,
          dateOfBirth: DateTime.fromJSDate(data.dateOfBirth).toISODate(),
          ...(emrexSessionId ? { emrexSessionId } : {}),
        }),
      }
    );

    if (!result.ok) {
      throw new Error('Failed to send data.');
    }

    const resultData = await result.json();

    useGeneralStore.setState({
      conversationId: resultData.conversationId,
      step: 0,
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-col space-y-4">
        <Input
          label="Name"
          variant="faded"
          errorMessage={errors.name?.message}
          id="name"
          type="text"
          {...register('name')}
        />
        <Input
          label="Surname"
          variant="faded"
          errorMessage={errors.surname?.message}
          id="surname"
          type="text"
          {...register('surname')}
        />
        <Input
          label="Date of birth"
          variant="faded"
          id="dateOfBirth"
          type="date"
          errorMessage={errors.dateOfBirth?.message}
          {...register('dateOfBirth')}
        />
        <Select
          variant="faded"
          fullWidth
          selectionMode="single"
          label="Emrex provider"
          {...register('provider')}
        >
          {emrexDataProviderOptions.map((option) => (
            <SelectItem key={option.value} value={option.value}>
              {option.label}
            </SelectItem>
          ))}
        </Select>
        <Button variant="shadow" color="primary" type="submit">
          Submit
        </Button>
      </div>
    </form>
  );
};
