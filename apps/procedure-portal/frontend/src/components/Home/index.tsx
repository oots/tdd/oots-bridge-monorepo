'use client';

import { useSearchParams } from 'next/navigation';
import { selectConversationId, selectStep, useGeneralStore } from '@/stores';
import { UserFormView } from './UserFormView';
import { Spinner } from '@nextui-org/react';
import { ResultsView } from './ResultsView';
import { useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';
import { SecondRequestView } from './SecondRequestView';
import { js2xml } from 'xml-js';
import { gzip } from 'pako';

const stepToComponent = (
  step: number,
  emrexDataProviderOptions: { label: string; value: string }[]
) => {
  switch (step) {
    case 1:
      // Provider selection view
      return (
        <UserFormView emrexDataProviderOptions={emrexDataProviderOptions} />
      );
    case 2:
      return <SecondRequestView />;
    case 3:
      // Results view
      return <ResultsView />;
    default:
      return (
        <div className="flex justify-center">
          <Spinner />
        </div>
      );
  }
};

export const Home = ({
  emrexDataProviderOptions,
}: {
  emrexDataProviderOptions: { label: string; value: string }[];
}) => {
  // Search params
  const searchParams = useSearchParams();

  // Global state
  const conversationId = useGeneralStore(selectConversationId);
  const step = useGeneralStore(selectStep);

  // Get conversationId from search params
  if (searchParams.get('conversationId')) {
    useGeneralStore.setState({
      conversationId: searchParams.get('conversationId')!,
    });
  }

  // Get emrexSessionId from search params if it exists
  if (searchParams.get('emrexSessionId')) {
    useGeneralStore.setState({
      emrexSessionId: searchParams.get('emrexSessionId')!,
    });
  }

  // Query previw status
  const { isPending, data } = useQuery<{
    previewLocation: string;
    elmo?: any;
    responseNumber: number;
    emrexSession?: {
      sessionId: string;
      returnUrl: string;
    };
    elmoXmlBase64?: string;
  }>({
    queryKey: ['messageData', conversationId],
    queryFn: async () => {
      if (!conversationId) {
        return Promise.resolve(null);
      }

      const response = await fetch(
        `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL}/message/${conversationId}`
      );

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();

      const elmoXmlBase64 = data?.elmo
        ? Buffer.from(
            gzip(
              js2xml(
                {
                  elmo: {
                    _attributes: {
                      xmlns: 'https://github.com/emrex-eu/elmo-schemas/tree/v1',
                      'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
                      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                      'xsi:schemaLocation':
                        'https://raw.githubusercontent.com/emrex-eu/elmo-schemas/v1/schema.xsd',
                    },
                    ...data.elmo,
                  },
                },
                {
                  compact: true,
                  spaces: 4,
                }
              )
            )
          ).toString('base64')
        : null;

      return { ...data, elmoXmlBase64 };
    },
    refetchInterval: 2000,
  });

  useEffect(() => {
    if (data) {
      useGeneralStore.setState({
        previewLocation: data.previewLocation,
        step:
          data.responseNumber === 1
            ? searchParams.get('conversationId')
              ? 0
              : 2
            : 3,
      });
    }
  }, [data]);

  useEffect(() => {
    if (step === 0 && !conversationId && !searchParams.get('conversationId')) {
      useGeneralStore.setState({ step: 1 });
    }
  }, [step, conversationId]);

  return isPending ? (
    <div className="flex justify-center">
      <Spinner />
    </div>
  ) : (
    stepToComponent(step, emrexDataProviderOptions)
  );
};
