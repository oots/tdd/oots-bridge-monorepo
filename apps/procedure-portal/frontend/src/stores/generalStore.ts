import { create } from 'zustand';

interface GeneralStore {
  conversationId: string;
  previewLocation: string;
  emrexSessionId: string;
  step: number;
}

export const useGeneralStore = create<GeneralStore>()(() => ({
  conversationId: '',
  previewLocation: '',
  emrexSessionId: '',
  step: 0,
}));

export const selectConversationId = (state: GeneralStore) =>
  state.conversationId;
export const selectPreviewLocation = (state: GeneralStore) =>
  state.previewLocation;
export const selectEmrexSessionId = (state: GeneralStore) =>
  state.emrexSessionId;
export const selectStep = (state: GeneralStore) => state.step;
