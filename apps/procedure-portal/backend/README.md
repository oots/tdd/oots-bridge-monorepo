# Procedure portal backend

This is the backend server for the Procedure portal.

### Health check

The health check is available at `/health`.
