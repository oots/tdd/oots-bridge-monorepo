import { randomUUID } from 'node:crypto';
import type { OOTSQueryRequest } from '@oots-emrex/utils';

export const DEFAULT_REQUEST_HEADER = {
  action: 'ExecuteQueryRequest',
  service: 'QueryManager',
  type: 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
};

export const DEFAULT_REQUEST_PAYLOAD: OOTSQueryRequest = {
  _declaration: {
    _attributes: {
      version: '1.0',
      encoding: 'utf-8',
    },
  },
  'query:QueryRequest': {
    _attributes: {
      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
      'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
      'xmlns:sdg': 'http://data.europa.eu/p4s',
      'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
      'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
      'xmlns:xlink': 'http://www.w3.org/1999/xlink',
      'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
      id: `urn:uuid:${randomUUID()}`,
      'xml:lang': 'EN',
    },
    'rim:Slot': [
      {
        _attributes: {
          name: 'SpecificationIdentifier',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: 'oots-edm:v1.0',
          },
        },
      },
      {
        _attributes: {
          name: 'IssueDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2021-02-14T19:20:30+01:00',
          },
        },
      },
      {
        _attributes: {
          name: 'Procedure',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:InternationalStringValueType',
          },
          'rim:Value': {
            'rim:LocalizedString': {
              _attributes: {
                'xml:lang': 'EN',
                value: 'V2',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'PossibilityForPreview',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'ExplicitRequestGiven',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'Requirements',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
            collectionType:
              'urn:oasis:names:tc:ebxml-regrep:CollectionType:Set',
          },
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Requirement': {
              'sdg:Identifier': {
                _text:
                  'https://sr.oots.tech.ec.europa.eu/requirements/cfcd4e22-30f1-48fa-93e4-6cf98cedbf40',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Proof of vehicle registration',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceRequester',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
            collectionType:
              'urn:oasis:names:tc:ebxml-regrep:CollectionType:Set',
          },
          'rim:Element': [
            {
              _attributes: {
                'xsi:type': 'rim:AnyValueType',
              },
              'sdg:Agent': {
                'sdg:Identifier': {
                  _attributes: {
                    schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                  },
                  _text: 'DK22233223',
                },
                'sdg:Name': [
                  {
                    _attributes: {
                      lang: 'EN',
                    },
                    _text: 'Test 1',
                  },
                  {
                    _attributes: {
                      lang: 'FR',
                    },
                    _text: 'Test 2',
                  },
                  {
                    _attributes: {
                      lang: 'DE',
                    },
                    _text: 'Test 3',
                  },
                ],
                'sdg:Address': {
                  'sdg:FullAddress': {
                    _text: 'Prince Street 15',
                  },
                  'sdg:LocatorDesignator': {
                    _text: '15',
                  },
                  'sdg:PostCode': {
                    _text: '1050',
                  },
                  'sdg:PostCityName': {
                    _text: 'Copenhagen',
                  },
                  'sdg:AdminUnitLevel1': {
                    _text: 'DK',
                  },
                  'sdg:AdminUnitLevel2': {
                    _text: 'DK011',
                  },
                },
                'sdg:Classification': {
                  _text: 'ER',
                },
              },
            },
            {
              _attributes: {
                'xsi:type': 'rim:AnyValueType',
              },
              'sdg:Agent': {
                'sdg:Identifier': {
                  _attributes: {
                    schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                  },
                  _text: 'DK22233223',
                },
                'sdg:Name': [
                  {
                    _attributes: {
                      lang: 'EN',
                    },
                    _text: 'Test 1',
                  },
                  {
                    _attributes: {
                      lang: 'FR',
                    },
                    _text: 'Test 2',
                  },
                  {
                    _attributes: {
                      lang: 'DE',
                    },
                    _text: 'Test 3',
                  },
                ],
                'sdg:Address': {
                  'sdg:FullAddress': {
                    _text: 'Prince Street 15',
                  },
                  'sdg:LocatorDesignator': {
                    _text: '15',
                  },
                  'sdg:PostCode': {
                    _text: '1050',
                  },
                  'sdg:PostCityName': {
                    _text: 'Copenhagen',
                  },
                  'sdg:AdminUnitLevel1': {
                    _text: 'DK',
                  },
                  'sdg:AdminUnitLevel2': {
                    _text: 'DK011',
                  },
                },
                'sdg:Classification': {
                  _text: 'IP',
                },
              },
            },
          ],
        },
      },
    ],
    'query:ResponseOption': {
      _attributes: {
        returnType: 'LeafClassWithRepositoryItem',
      },
    },
    'query:Query': {
      _attributes: {
        queryDefinition: 'DocumentQuery',
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'EvidenceRequest',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:DataServiceEvidenceType': {
              'sdg:Identifier': {
                _text: 'aa55b273-0698-4a96-91cc-4d980055c0e1',
              },
              'sdg:EvidenceTypeClassification': {
                _text:
                  'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DK/4a51a392-79a8-437b-b53c-8cddbf7210b5',
              },
              'sdg:Title': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Vehicle registration evidence',
              },
              'sdg:Description': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Proof of vehicle registration.',
              },
              'sdg:DistributedAs': {
                'sdg:Format': {
                  _text: 'application/xml',
                },
                'sdg:ConformsTo': {
                  _text:
                    'https://sr.oots.tech.ec.europa.eu/distributions/secondary-education-evidence-1.0.0',
                },
                'sdg:Transformation': {
                  _text:
                    'https://sr.oots.tech.ec.europa.eu/distributions/secondary-education-evidence-1.0.0/passed',
                },
              },
            },
          },
        },
      ],
    },
  },
};
