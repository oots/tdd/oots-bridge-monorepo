import * as path from 'node:path';
import { fileURLToPath } from 'node:url';
import AutoLoad, { type AutoloadPluginOptions } from '@fastify/autoload';
import type { FastifyPluginAsync, FastifyServerOptions } from 'fastify';
import { fastifyUnderPressure } from '@fastify/under-pressure';
import fastifyHealthcheck from 'fastify-healthcheck';
import fastifyPrintRoutes from 'fastify-print-routes';

const filename = fileURLToPath(import.meta.url);

const dirname = path.dirname(filename);

export interface AppOptions
  extends FastifyServerOptions,
    Partial<AutoloadPluginOptions> {}

const options: AppOptions = {};

const app: FastifyPluginAsync<AppOptions> = async (
  fastify,
  _opts
): Promise<void> => {
  await fastify.register(fastifyPrintRoutes);
  await fastify.register(fastifyHealthcheck);

  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  await fastify.register(AutoLoad, {
    dir: path.join(dirname, 'plugins'),
    forceESM: true,
  });

  // This loads all REST routes
  await fastify.register(AutoLoad, {
    dir: path.join(dirname, 'routes'),
    forceESM: true,
  });

  // Load Under-Pressure plugin
  await fastify.register(fastifyUnderPressure, {
    maxEventLoopDelay: 1000,
    maxHeapUsedBytes: 1000000000,
    maxRssBytes: 1000000000,
    maxEventLoopUtilization: 0.98,
    message: 'Under pressure!',
    retryAfter: 50,
  });
};

export default app;
export { app, options };
