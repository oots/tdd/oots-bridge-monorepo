import type { FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';

interface StoredObject<T> {
  created: number;
  data: T;
}

interface MessageLog {
  requestData: {
    name: string;
    surname: string;
    dateOfBirth: string;
    provider: string;
  };
  domibusResponseData: {
    header: any;
    body: any;
  };
  previewLocation: string | null;
  elmo: any;
  responseNumber: number;
  emrexSessionId?: string;
}
interface EmrexSession {
  sessionId: string; // ID we get from Emrex Client
  returnUrl: string; // URL where we need to redirect the user (POST)
}

class StorageService {
  messageLogStorage: Map<string, StoredObject<MessageLog>>;
  emrexSessionStorage: Map<string, StoredObject<EmrexSession>>;

  constructor() {
    this.messageLogStorage = new Map<string, any>();
    this.emrexSessionStorage = new Map<string, any>();
  }

  getMessageLogById = (id: string) => {
    const stored = this.messageLogStorage.get(id);

    if (stored) {
      return stored.data;
    }

    return null;
  };

  createMessageLog = (id: string, emrexSessionId?: string) => {
    this.messageLogStorage.set(id, {
      created: Date.now(),
      data: {
        requestData: {
          name: '',
          surname: '',
          dateOfBirth: '',
          provider: '',
        },
        domibusResponseData: { header: null, body: null },
        previewLocation: null,
        elmo: null,
        responseNumber: 0,
        emrexSessionId,
      },
    });
  };

  updateMessageLog = (id: string, data: Partial<MessageLog>) => {
    const stored = this.messageLogStorage.get(id);

    if (stored) {
      stored.data = {
        ...stored.data,
        ...data,
      };
    }
  };

  getEmrexSessionById = (id: string) => {
    const stored = this.emrexSessionStorage.get(id);

    if (stored) {
      return stored.data;
    }

    return null;
  };

  createEmrexSession = (id: string, returnUrl: string) => {
    this.emrexSessionStorage.set(id, {
      created: Date.now(),
      data: {
        sessionId: id,
        returnUrl,
      },
    });
  };

  clear() {
    this.messageLogStorage.clear();
  }
}

export default fp<FastifyPluginOptions>(async (fastify, _opts) => {
  const storage = new StorageService();

  fastify.decorate('storage', storage);
});

declare module 'fastify' {
  export interface FastifyInstance {
    storage: StorageService;
  }
}
