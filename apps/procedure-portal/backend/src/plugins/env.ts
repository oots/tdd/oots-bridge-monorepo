import envSchema, { type JSONSchemaType } from 'env-schema';
import fp from 'fastify-plugin';
import axios, { type AxiosInstance } from 'axios';
import { HttpsProxyAgent } from 'https-proxy-agent';

export interface Env {
  // Domibus access point
  DOMIBUS_ACCESS_POINT: string;
  ACCESS_POINT_PARTY_ID: string;
  ACCESS_POINT_PARTY_TYPE: string;

  // Procedure portal party information
  PARTY_ID: string;

  // Emrex data providers
  EMREX_DATA_PROVIDERS: string;

  // Forward proxy URL
  PROXY_URL: string;

  // Bridge party information
  BRIDGE_ACCESS_POINT_PARTY_ID: string;
  BRIDGE_ACCESS_POINT_PARTY_TYPE: string;
  BRIDGE_PARTY_ID: string;

  // Procedure portal frontend URL
  PROCEDURE_PORTAL_FRONTEND_URL: string;
}

declare module 'fastify' {
  export interface FastifyInstance {
    config: {
      DOMIBUS_ACCESS_POINT: string;
      ACCESS_POINT_PARTY_ID: string;
      ACCESS_POINT_PARTY_TYPE: string;
      PARTY_ID: string;
      EMREX_DATA_PROVIDERS: { [key: string]: string };
      AXIOS_INSTANCE: AxiosInstance;
      BRIDGE_ACCESS_POINT_PARTY_ID: string;
      BRIDGE_ACCESS_POINT_PARTY_TYPE: string;
      BRIDGE_PARTY_ID: string;
      PROCEDURE_PORTAL_FRONTEND_URL: string;
    };
  }
}

/**
 * This plugins adds some utilities to handle loading ENV variables
 *
 * @see https://github.com/fastify/fastify-env
 */
export default fp(async (fastify, _) => {
  const schema: JSONSchemaType<Env> = {
    type: 'object',
    properties: {
      DOMIBUS_ACCESS_POINT: {
        type: 'string',
      },
      ACCESS_POINT_PARTY_ID: {
        type: 'string',
      },
      ACCESS_POINT_PARTY_TYPE: {
        type: 'string',
      },
      PARTY_ID: {
        type: 'string',
      },
      EMREX_DATA_PROVIDERS: {
        type: 'string',
      },
      PROXY_URL: {
        type: 'string',
      },
      BRIDGE_ACCESS_POINT_PARTY_ID: {
        type: 'string',
      },
      BRIDGE_ACCESS_POINT_PARTY_TYPE: {
        type: 'string',
      },
      BRIDGE_PARTY_ID: {
        type: 'string',
      },
      PROCEDURE_PORTAL_FRONTEND_URL: {
        type: 'string',
      },
    },
    required: [
      'DOMIBUS_ACCESS_POINT',
      'ACCESS_POINT_PARTY_ID',
      'ACCESS_POINT_PARTY_TYPE',
      'PARTY_ID',
      'EMREX_DATA_PROVIDERS',
      'BRIDGE_ACCESS_POINT_PARTY_ID',
      'BRIDGE_ACCESS_POINT_PARTY_TYPE',
      'BRIDGE_PARTY_ID',
      'PROCEDURE_PORTAL_FRONTEND_URL',
    ],
  };

  const envConfig = envSchema({
    schema,
    dotenv: true,
  });

  const proxy = envConfig.PROXY_URL
    ? new HttpsProxyAgent(envConfig.PROXY_URL)
    : undefined;

  const axiosInstance = axios.create({
    httpsAgent: proxy,
    httpAgent: proxy,
  });

  const config = {
    DOMIBUS_ACCESS_POINT: envConfig.DOMIBUS_ACCESS_POINT,
    ACCESS_POINT_PARTY_ID: envConfig.ACCESS_POINT_PARTY_ID,
    ACCESS_POINT_PARTY_TYPE: envConfig.ACCESS_POINT_PARTY_TYPE,
    PARTY_ID: envConfig.PARTY_ID,
    EMREX_DATA_PROVIDERS: JSON.parse(envConfig.EMREX_DATA_PROVIDERS),
    AXIOS_INSTANCE: axiosInstance,
    BRIDGE_ACCESS_POINT_PARTY_ID: envConfig.BRIDGE_ACCESS_POINT_PARTY_ID,
    BRIDGE_ACCESS_POINT_PARTY_TYPE: envConfig.BRIDGE_ACCESS_POINT_PARTY_TYPE,
    BRIDGE_PARTY_ID: envConfig.BRIDGE_PARTY_ID,
    PROCEDURE_PORTAL_FRONTEND_URL: envConfig.PROCEDURE_PORTAL_FRONTEND_URL,
  };

  fastify.decorate('config', config);
});
