import type { FastifyPluginAsync } from 'fastify';
import { AsyncTask, SimpleIntervalJob } from 'toad-scheduler';
import { fastifySchedule } from '@fastify/schedule';
import {
  isError,
  listPendingMessages,
  retrieveMessage,
} from '@oots-emrex/utils';
import { xml2json } from 'xml-js';
import { randomUUID } from 'node:crypto';

const plugin: FastifyPluginAsync = async (fastify): Promise<void> => {
  const task = new AsyncTask(
    'Request processing task',
    async () => {
      const listPendingMessagesResult = await listPendingMessages(
        fastify.config.DOMIBUS_ACCESS_POINT
      );
      // Check for errors
      if (isError(listPendingMessagesResult)) {
        fastify.log.error({
          id: randomUUID(),
          messageId: null,
          conversationId: null,
          operation: 'LIST_PENDING_MESSAGES_ERROR',
          error: listPendingMessagesResult.error,
        });
        return;
      }
      const { messageIds } = listPendingMessagesResult.data;
      await Promise.all(
        messageIds.map(async (messageId) => {
          fastify.log.info({
            id: randomUUID(),
            messageId: messageId,
            conversationId: null,
            operation: 'PROCESSING_MESSAGE',
            error: null,
          });
          // Retrieve message
          const messageResult = await retrieveMessage(
            fastify.config.DOMIBUS_ACCESS_POINT,
            messageId
          );
          fastify.log.info({
            id: randomUUID(),
            messageId: messageId,
            conversationId: null,
            operation: 'RETRIEVED_MESSAGE',
            error: null,
          });
          // Check for errors
          if (isError(messageResult)) {
            fastify.log.error({
              id: randomUUID(),
              messageId: messageId,
              conversationId: null,
              operation: 'RETRIEVE_MESSAGE_ERROR',
              error: messageResult.error,
            });
            return;
          }
          const { header, body } = messageResult.data;
          // ConversationId is required
          if (!header.Messaging.UserMessage.CollaborationInfo.ConversationId) {
            fastify.log.error({
              id: randomUUID(),
              messageId: messageId,
              conversationId: null,
              operation: 'CONVERSATION_ID_NOT_FOUND',
              error: null,
            });
            return;
          }
          // Get message log
          const messageLog = fastify.storage.getMessageLogById(
            header.Messaging.UserMessage.CollaborationInfo.ConversationId
          );
          if (!messageLog) {
            fastify.log.error({
              id: randomUUID(),
              messageId: messageId,
              conversationId: header.Messaging.UserMessage.CollaborationInfo,
              operation: 'MESSAGE_LOG_NOT_FOUND',
              error: null,
            });
            return;
          }
          // If elmo is not set and this is the second response, extract it from the message
          if (!messageLog.elmo && messageLog.responseNumber === 1) {
            fastify.storage.updateMessageLog(
              header.Messaging.UserMessage.CollaborationInfo.ConversationId,
              {
                responseNumber: 2,
              }
            );
            // Extract elmo payload from domibus body
            const elmoPayload = body.payload[1]?.value;
            if (!elmoPayload) {
              return;
            }
            const payloadJSON = JSON.parse(
              xml2json(
                Buffer.from(body.payload[1].value, 'base64').toString('utf-8'),
                {
                  compact: true,
                  spaces: 4,
                }
              )
            );
            fastify.storage.updateMessageLog(
              header.Messaging.UserMessage.CollaborationInfo.ConversationId,
              {
                elmo: payloadJSON.elmo,
              }
            );
          }
          // If domibus request data is not set, set it
          if (
            !messageLog.domibusResponseData.body ||
            !messageLog.domibusResponseData.header
          ) {
            fastify.storage.updateMessageLog(
              header.Messaging.UserMessage.CollaborationInfo.ConversationId,
              {
                domibusResponseData: {
                  header,
                  body,
                },
                responseNumber: 1,
              }
            );
          }
          // If preview location is not set, extract it from the message
          if (!messageLog.previewLocation) {
            // Extract oots message data from domibus body
            const message = JSON.parse(
              xml2json(
                Buffer.from(body.payload[0].value, 'base64').toString('utf-8'),
                {
                  compact: true,
                  spaces: 4,
                }
              )
            );
            const rimSlots =
              message['query:QueryResponse']['rs:Exception']['rim:Slot'];
            const previewLocationSlot = rimSlots.find(
              (slot: any) => slot._attributes.name === 'PreviewLocation'
            );
            if (!previewLocationSlot) {
              fastify.log.error({
                id: randomUUID(),
                messageId: messageId,
                conversationId:
                  header.Messaging.UserMessage.CollaborationInfo.ConversationId,
                operation: 'PREVIEW_LOCATION_SLOT_NOT_FOUND_IN_MESSAGE',
                error: null,
              });
              return;
            }
            const previewLocation =
              previewLocationSlot['rim:SlotValue']['rim:Value']._text;
            if (!previewLocation) {
              fastify.log.error({
                id: randomUUID,
                messageId: messageId,
                conversationId:
                  header.Messaging.UserMessage.CollaborationInfo.ConversationId,
                operation: 'PREVIEW_LOCATION_MISSING_IN_MESSAGE',
                error: null,
              });
              return;
            }
            fastify.storage.updateMessageLog(
              header.Messaging.UserMessage.CollaborationInfo.ConversationId,
              {
                previewLocation,
              }
            );
          }
          fastify.log.info({
            id: randomUUID(),
            messageId: messageId,
            conversationId:
              header.Messaging.UserMessage.CollaborationInfo.ConversationId,
            operation: 'MESSAGE_PROCESSED',
            error: null,
          });
        })
      );
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );
  // Create the job
  const job = new SimpleIntervalJob({ seconds: 1 }, task);
  // Register the scheduler
  await fastify.register(fastifySchedule);
  // Start the scheduler
  fastify.ready().then(
    () => {
      fastify.scheduler.addSimpleIntervalJob(job);
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );
};

export default plugin;
