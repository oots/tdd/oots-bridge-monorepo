import type { FastifyPluginAsync } from 'fastify';

const root: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  fastify.get('/', async () => {
    const response = { root: true };

    return response;
  });
};

export default root;
