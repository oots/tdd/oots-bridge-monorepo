import type { PersonSlot } from '@oots-emrex/utils';

interface CreatePersonSlotParams {
  surname: string;
  name: string;
  dateOfBirth: string;
}

export const createPersonSlot = ({
  surname,
  name,
  dateOfBirth,
}: CreatePersonSlotParams): PersonSlot => ({
  _attributes: {
    name: 'NaturalPerson',
  },
  'rim:SlotValue': {
    _attributes: {
      'xsi:type': 'rim:AnyValueType',
    },
    'sdg:Person': {
      'sdg:LevelOfAssurance': {
        _text: 'High',
      },
      'sdg:FamilyName': {
        _text: surname,
      },
      'sdg:GivenName': {
        _text: name,
      },
      'sdg:DateOfBirth': {
        _text: dateOfBirth,
      },
    },
  },
});
