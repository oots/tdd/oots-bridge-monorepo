const PROVIDERS = new Map<string, string>([
  ['DEMO1', '[NL] Netherlands test EMP'],
  ['DEMO2', '[NO] Norway test EMP'],
  ['DEMO3', '[SE] Sweden test EMP'],
]);

export const createEvidenceProvider = (provider: string) => {
  const providerName = PROVIDERS.get(provider);

  if (!providerName) {
    throw new Error(`Provider ${provider} not found`);
  }

  return {
    _attributes: {
      name: 'EvidenceProvider',
    },
    'rim:SlotValue': {
      _attributes: {
        'xsi:type': 'rim:AnyValueType',
      },
      'sdg:Agent': {
        'sdg:Identifier': {
          _attributes: {
            schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
          },
          _text: provider,
        },
        'sdg:Name': [
          {
            _attributes: {
              lang: 'EN',
            },
            _text: 'Test 1',
          },
          {
            _attributes: {
              lang: 'FR',
            },
            _text: 'Test 2',
          },
          {
            _attributes: {
              lang: 'DE',
            },
            _text: 'Test 3',
          },
        ],
      },
    },
  };
};
