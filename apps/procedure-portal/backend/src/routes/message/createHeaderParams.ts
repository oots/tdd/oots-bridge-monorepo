import type { CreateHeaderParams, PartyInfo } from '@oots-emrex/utils';
import { DEFAULT_REQUEST_HEADER } from '../../utils/constants.js';

export const createHeaderParams = ({
  conversationId,
  fromParty,
  toParty,
  originalSender,
  finalRecipient,
}: {
  conversationId: string;
  fromParty: PartyInfo;
  toParty: PartyInfo;
  originalSender: {
    value: string;
    type: string;
  };
  finalRecipient: {
    value: string;
    type: string;
  };
}): CreateHeaderParams => ({
  fromParty,
  toParty,
  collaborationInfo: {
    action: DEFAULT_REQUEST_HEADER.action,
    service: DEFAULT_REQUEST_HEADER.service,
    type: DEFAULT_REQUEST_HEADER.type,
    conversationId,
  },
  messageInfo: {
    originalSender,
    finalRecipient,
  },
});
