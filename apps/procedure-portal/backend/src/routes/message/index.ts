import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';
import { randomUUID } from 'node:crypto';
import { js2xml } from 'xml-js';
import {
  type CreateBodyParams,
  isError,
  submitMessage,
  type CreateHeaderParams,
} from '@oots-emrex/utils';
import { createPersonSlot } from './createPersonSlot.js';
import { createHeaderParams } from './createHeaderParams.js';
import { DEFAULT_REQUEST_PAYLOAD } from '../../utils/constants.js';
import { createEvidenceProvider } from './createEvidenceProvider.js';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  fastify.get(
    '/:id',
    {
      schema: {
        params: {
          type: 'object',
          properties: {
            id: { type: 'string' },
          },
          required: ['id'],
        } as const,
      },
    },
    (request, reply) => {
      const messageLog = fastify.storage.getMessageLogById(request.params.id);

      if (!messageLog) return reply.code(404).send();

      const { previewLocation, elmo, responseNumber } = messageLog;

      if (!previewLocation) {
        return reply.code(404).send({
          error: 'Preview location not found',
        });
      }

      // Get emrex session if one is bound to the messageLog
      let emrexSession = null;
      if (messageLog.emrexSessionId) {
        emrexSession = fastify.storage.getEmrexSessionById(
          messageLog.emrexSessionId
        );
      }

      return reply.code(200).send({
        previewLocation,
        elmo,
        responseNumber,
        ...(emrexSession ? { emrexSession } : {}),
      });
    }
  );

  fastify.post(
    '/',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            name: { type: 'string' },
            surname: { type: 'string' },
            dateOfBirth: { type: 'string' },
            provider: { type: 'string' },
            emrexSessionId: { type: 'string' },
          },
          required: ['name', 'surname', 'dateOfBirth', 'provider'],
        } as const,
      },
    },
    async (request, reply) => {
      const { name, surname, dateOfBirth, emrexSessionId } = request.body;

      // Use conversationId if it exists, otherwise generate a new one
      const conversationId = randomUUID();

      const headerParams: CreateHeaderParams = createHeaderParams({
        conversationId,
        fromParty: {
          id: fastify.config.ACCESS_POINT_PARTY_ID,
          type: fastify.config.ACCESS_POINT_PARTY_TYPE,
          role: 'http://sdg.europa.eu/edelivery/gateway',
        },
        toParty: {
          id: fastify.config.BRIDGE_ACCESS_POINT_PARTY_ID,
          type: fastify.config.BRIDGE_ACCESS_POINT_PARTY_TYPE,
          role: 'http://sdg.europa.eu/edelivery/gateway',
        },
        originalSender: {
          value: fastify.config.PARTY_ID,
          type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
        },
        finalRecipient: {
          value: fastify.config.BRIDGE_PARTY_ID,
          type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
        },
      });

      const personSlot = createPersonSlot({
        surname,
        name,
        dateOfBirth,
      });

      const payload = structuredClone(DEFAULT_REQUEST_PAYLOAD);

      // Add person slot to payload
      payload['query:QueryRequest']['query:Query']['rim:Slot'].push(personSlot);

      // Add requested evidence provider to payload
      payload['query:QueryRequest']['rim:Slot'].push(
        createEvidenceProvider(request.body.provider)
      );

      const payloadXML = js2xml(payload, {
        compact: true,
        spaces: 4,
      });

      const bodyParams: CreateBodyParams = {
        payload: {
          parts: [
            {
              href: `${randomUUID()}@c2.example.com`,
              contentType: 'application/x-ebrs+xml',
              value: Buffer.from(payloadXML).toString('base64'),
            },
          ],
        },
      };

      const result = await submitMessage({
        url: fastify.config.DOMIBUS_ACCESS_POINT,
        createHeaderParams: headerParams,
        createBodyParams: bodyParams,
      });

      if (isError(result)) {
        fastify.log.error({
          id: randomUUID(),
          messageId: null,
          conversationId,
          operation: 'SUBMIT_MESSAGE_ERROR',
          error: result.error,
        });

        return reply.code(500).send({
          error: 'An error occurred while submiting the message to domibus',
        });
      }

      fastify.log.info({
        id: randomUUID(),
        messageId: null,
        conversationId,
        operation: 'MESSAGE_SUBMITTED',
        error: null,
      });

      fastify.storage.createMessageLog(conversationId, emrexSessionId);

      fastify.storage.updateMessageLog(conversationId, {
        requestData: {
          name,
          surname,
          dateOfBirth,
          provider: request.body.provider,
        },
      });

      return reply.code(201).send({ ...result.data, conversationId });
    }
  );

  fastify.post(
    '/:id',
    {
      schema: {
        params: {
          type: 'object',
          properties: {
            id: { type: 'string' },
          },
          required: ['id'],
        } as const,
      },
    },
    async (request, reply) => {
      const conversationId = request.params.id;

      const messageLog = fastify.storage.getMessageLogById(conversationId);

      if (!messageLog) {
        return reply.code(404).send({
          error: 'ConversationId does not exist',
        });
      }

      if (!messageLog.previewLocation) {
        return reply.code(404).send({
          error: 'Preview location not found',
        });
      }

      const headerParams = createHeaderParams({
        conversationId,
        fromParty: {
          id: fastify.config.ACCESS_POINT_PARTY_ID,
          type: fastify.config.ACCESS_POINT_PARTY_TYPE,
          role: 'http://sdg.europa.eu/edelivery/gateway',
        },
        toParty: {
          id: fastify.config.BRIDGE_ACCESS_POINT_PARTY_ID,
          type: fastify.config.BRIDGE_ACCESS_POINT_PARTY_TYPE,
          role: 'http://sdg.europa.eu/edelivery/gateway',
        },
        originalSender: {
          value: fastify.config.PARTY_ID,
          type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
        },
        finalRecipient: {
          value: fastify.config.BRIDGE_PARTY_ID,
          type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
        },
      });

      const personSlot = createPersonSlot(messageLog.requestData);

      const payload = structuredClone(DEFAULT_REQUEST_PAYLOAD);

      // Add person slot to payload
      payload['query:QueryRequest']['query:Query']['rim:Slot'].push(personSlot);

      // Add preview location
      payload['query:QueryRequest']['rim:Slot'].push({
        _attributes: {
          name: 'PreviewLocation',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: messageLog.previewLocation,
          },
        },
      });

      // Add requested evidence provider to payload
      payload['query:QueryRequest']['rim:Slot'].push(
        createEvidenceProvider(messageLog.requestData.provider)
      );

      const payloadXML = js2xml(payload, {
        compact: true,
        spaces: 4,
      });

      const bodyParams: CreateBodyParams = {
        payload: {
          parts: [
            {
              href: `${randomUUID()}@c2.example.com`,
              contentType: 'application/x-ebrs+xml',
              value: Buffer.from(payloadXML).toString('base64'),
            },
          ],
        },
      };

      const result = await submitMessage({
        url: fastify.config.DOMIBUS_ACCESS_POINT,
        createHeaderParams: headerParams,
        createBodyParams: bodyParams,
      });

      if (isError(result)) {
        fastify.log.error({
          id: randomUUID(),
          messageId: null,
          conversationId,
          operation: 'SUBMIT_MESSAGE_ERROR',
          error: result.error,
        });

        return reply.code(500).send({
          error: 'An error occurred while submiting the message to domibus',
        });
      }

      fastify.log.info({
        id: randomUUID(),
        messageId: null,
        conversationId,
        operation: 'MESSAGE_SUBMITTED',
        error: null,
      });

      return reply.code(204).send();
    }
  );
};

export default plugin;
