import type { FastifyPluginAsyncJsonSchemaToTs } from '@fastify/type-provider-json-schema-to-ts';
import { fastifyFormbody } from '@fastify/formbody';

const plugin: FastifyPluginAsyncJsonSchemaToTs = async (
  fastify
): Promise<void> => {
  await fastify.register(fastifyFormbody, { bodyLimit: 1048576 * 10 });

  // TODO: Better schema validation
  fastify.post(
    '/',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            sessionId: { type: 'string' },
            returnUrl: { type: 'string' },
          },
          required: ['sessionId', 'returnUrl'],
        } as const,
      },
    },
    async (request, reply) => {
      const { sessionId, returnUrl } = request.body;

      fastify.storage.createEmrexSession(sessionId, returnUrl);

      return reply.redirect(
        `${fastify.config.PROCEDURE_PORTAL_FRONTEND_URL}?emrexSessionId=${sessionId}`
      );
    }
  );
};

export default plugin;
