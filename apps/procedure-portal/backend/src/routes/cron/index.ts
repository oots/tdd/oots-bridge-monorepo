import fastifySchedule from '@fastify/schedule';
import type { FastifyPluginAsync } from 'fastify';
import { randomUUID } from 'node:crypto';
import { AsyncTask, CronJob } from 'toad-scheduler';

const plugin: FastifyPluginAsync = async (fastify): Promise<void> => {
  const task = new AsyncTask(
    'Storage cleanup task',
    async () => {
      fastify.log.info({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'CLEANUP_STORAGE',
        error: null,
      });

      fastify.storage.clear();
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );

  // Create the job
  const job = new CronJob(
    {
      // Runs every day at 01:00
      cronExpression: '* 1 * * *',
    },
    task,
    {
      preventOverrun: true,
    }
  );

  // Register the scheduler
  await fastify.register(fastifySchedule);

  // Start the scheduler
  fastify.ready().then(
    () => {
      fastify.scheduler.addCronJob(job);
    },
    (err) => {
      fastify.log.error({
        id: randomUUID(),
        messageId: null,
        conversationId: null,
        operation: 'UNEXPECTED_ERROR',
        error: JSON.stringify(err),
      });
    }
  );
};

export default plugin;
