import type { PayloadInfo } from './types';

export type CreateBodyParams = {
  payload: PayloadInfo;
};

export type DomibusMessageBodyPayload = {
  attributes: {
    payloadId: string;
    contentType: string;
  };
  value: string;
};

export type DomibusMessageBody = {
  payload: DomibusMessageBodyPayload[];
};

export const createBody = ({
  payload,
}: CreateBodyParams): DomibusMessageBody => ({
  payload: payload.parts.map((part) => ({
    attributes: {
      payloadId: `cid:${part.href}`,
      contentType: part.contentType,
    },
    value: part.value,
  })),
});
