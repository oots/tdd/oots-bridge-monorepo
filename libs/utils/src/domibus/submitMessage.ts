import { createClientAsync } from 'soap';
import type { Result } from '../results.js';
import { createHeader, type CreateHeaderParams } from './createHeader.js';
import { type CreateBodyParams, createBody } from './createBody.js';

export type SubmitMessageParams = {
  url: string;
  createHeaderParams: CreateHeaderParams;
  createBodyParams: CreateBodyParams;
};

/**
 * Function to submit a message to a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 * @param createHeaderParams - The header parameters from which to create the message header
 * @param createBodyParams - The  ody parameters from which to create the message body
 */
export const submitMessage = async ({
  url,
  createHeaderParams,
  createBodyParams,
}: SubmitMessageParams): Promise<
  Result<{
    messageId: string;
  }>
> => {
  const client = await createClientAsync(`${url}?wsdl`);

  // Add the header to the SOAP request
  client.addSoapHeader(
    createHeader(createHeaderParams, createBodyParams.payload),
    '',
    'ns1'
  );

  try {
    // Submit the message to the Domibus endpoint with the message body
    const res = await client.submitMessageAsync(createBody(createBodyParams));

    return {
      success: true,
      data: { messageId: res[0].messageID[0] },
    };
  } catch (err) {
    return {
      success: false,
      error: err as any,
    };
  }
};
