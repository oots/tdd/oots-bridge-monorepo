import { createClientAsync } from 'soap';
import type { Result } from '../results.js';
import type { DomibusMessageHeader } from './createHeader.js';

export type RetrieveMessageResponse = {
  header: DomibusMessageHeader;
  body: any;
};

/**
 * Function to retrieve a message from a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 * @param messageId - The ID of the message to retrieve
 */
export const retrieveMessage = async (
  url: string,
  messageId: string
): Promise<Result<RetrieveMessageResponse>> => {
  const client = await createClientAsync(`${url}?wsdl`);

  try {
    const res = await client.retrieveMessageAsync({
      messageID: messageId,
    });

    return {
      success: true,
      data: {
        header: res[2],
        body: res[0],
      },
    };
  } catch (err) {
    return {
      success: false,
      error: err as any,
    };
  }
};
