export type PartyInfo = {
  id: string;
  role: string;
  type: string;
};

export type CollaborationInfo = {
  service: string;
  action: string;
  type: string;
  conversationId: string;
};

export type MessageInfo = {
  originalSender: {
    value: string;
    type: string;
  };
  finalRecipient: {
    value: string;
    type: string;
  };
};

export type PartInfo = {
  href: string;
  contentType: string;
  value: string;
};

export type PayloadInfo = {
  parts: PartInfo[];
};
