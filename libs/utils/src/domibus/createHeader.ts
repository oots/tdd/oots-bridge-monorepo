import { randomUUID } from 'node:crypto';

import type {
  CollaborationInfo,
  MessageInfo,
  PartyInfo,
  PayloadInfo,
} from './types';

export type CreateHeaderParams = {
  fromParty: PartyInfo;
  toParty: PartyInfo;
  collaborationInfo: CollaborationInfo;
  messageInfo: MessageInfo;
};

export type DomibusMessageHeader = {
  Messaging: {
    UserMessage: {
      MessageInfo?: {
        Timestamp?: string;
        MessageId?: string;
        RefToMessageId?: string;
      };
      PartyInfo: {
        From: {
          PartyId: {
            attributes: {
              type: string;
            };
            $value: string;
          };
          Role: string;
        };
        To: {
          PartyId: {
            attributes: {
              type: string;
            };
            $value: string;
          };
          Role: string;
        };
      };
      CollaborationInfo: {
        Service: {
          attributes: {
            type: string;
          };
          $value: string;
        };
        Action: string;
        ConversationId?: string;
      };
      MessageProperties: {
        Property: {
          attributes: {
            name: string;
            type: string;
          };
          $value: string;
        }[];
      };
      PayloadInfo: {
        PartInfo: {
          attributes: {
            href: string;
          };
          PartProperties: {
            Property: {
              attributes: {
                name: string;
              };
              $value: string;
            }[];
          };
        }[];
      };
    };
  };
};

export const createHeader = (
  createHeaderParams: CreateHeaderParams,
  payload: PayloadInfo
): DomibusMessageHeader => {
  const { fromParty, toParty, collaborationInfo, messageInfo } =
    createHeaderParams;

  return {
    Messaging: {
      UserMessage: {
        MessageInfo: {
          MessageId: `${randomUUID()}@oots-emrex.eu`,
        },
        PartyInfo: {
          From: {
            PartyId: {
              attributes: {
                type: fromParty.type,
              },
              $value: fromParty.id,
            },
            Role: fromParty.role,
          },
          To: {
            PartyId: {
              attributes: {
                type: toParty.type,
              },
              $value: toParty.id,
            },
            Role: toParty.role,
          },
        },
        CollaborationInfo: {
          Service: {
            attributes: {
              type: collaborationInfo.type,
            },
            $value: collaborationInfo.service,
          },
          Action: collaborationInfo.action,
          ConversationId: collaborationInfo.conversationId,
        },
        MessageProperties: {
          Property: [
            {
              attributes: {
                name: 'originalSender',
                type: messageInfo.originalSender.type,
              },
              $value: messageInfo.originalSender.value,
            },
            {
              attributes: {
                name: 'finalRecipient',
                type: messageInfo.finalRecipient.type,
              },
              $value: messageInfo.finalRecipient.value,
            },
          ],
        },
        PayloadInfo: {
          PartInfo: payload.parts.map((part) => ({
            attributes: {
              href: `cid:${part.href}`,
            },
            PartProperties: {
              Property: [
                {
                  attributes: {
                    name: 'MimeType',
                  },
                  $value: part.contentType,
                },
              ],
            },
          })),
        },
      },
    },
  };
};
