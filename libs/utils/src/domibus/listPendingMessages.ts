import { createClientAsync } from 'soap';
import type { Result } from '../results.js';

export type ListPendingMessagesResponse = {
  messageIds: string[];
};

/**
 * Function to list pending messages at a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 */
export const listPendingMessages = async (
  url: string
): Promise<Result<ListPendingMessagesResponse>> => {
  const client = await createClientAsync(`${url}?wsdl`);

  try {
    const res = await client.listPendingMessagesAsync({});
    return {
      success: true,
      data: { messageIds: res[0] ? res[0].messageID : [] },
    };
  } catch (err) {
    return {
      success: false,
      error: err as any,
    };
  }
};
