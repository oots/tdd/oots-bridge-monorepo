import type { AxiosInstance } from 'axios';

export const convertElmToElmo = async (
  url: string,
  elm: string,
  axiosInstance: AxiosInstance
): Promise<string> => {
  const payload = {
    From: { Name: 'elm', Version: '3.0' }, // To ELM 3.0
    To: { Name: 'elmo', Version: '1.7' }, // From ELMO 1.7
    Parameters: { PreferredLanguages: ['en', 'sv'] }, // Preferred languages
    Content: elm,
  };

  const result = await axiosInstance.post(`${url}/rest/request`, payload);

  if (!result.data) {
    throw new Error('Failed to convert to ELM to ELMO');
  }

  return result.data.Content;
};
