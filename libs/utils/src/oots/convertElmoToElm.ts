import type { AxiosInstance } from 'axios';

export const convertElmoToElm = async (
  url: string,
  elmo: string,
  axiosInstance: AxiosInstance
): Promise<string> => {
  const payload = {
    From: { Name: 'elmo', Version: '1.7' }, // From ELMO 1.7
    To: { Name: 'elm', Version: '3.0' }, // To ELM 3.0
    Parameters: { PreferredLanguages: ['en', 'sv'] }, // Preferred languages
    Content: elmo,
  };

  const result = await axiosInstance.post(`${url}/rest/request`, payload);

  if (!result.data) {
    throw new Error('Failed to convert to ELMO to ELM');
  }

  return result.data.Content;
};
