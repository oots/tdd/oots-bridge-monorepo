/**
 * Based on:
 * https://ec.europa.eu/digital-building-blocks/sites/pages/viewpage.action?pageId=706382123
 */
export interface OOTSQueryRequest {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryRequest': {
    _attributes: {
      'xmlns:xsi': string;
      'xmlns:rs': string;
      'xmlns:sdg': string;
      'xmlns:rim': string;
      'xmlns:query': string;
      'xmlns:xlink': string;
      'xmlns:xml': string;
      id: string;
      'xml:lang'?: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          'rim:LocalizedString'?: any;
          _text?: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
        'sdg:DataServiceEvidenceType'?: any;
      };
    }[];
    'query:ResponseOption': {
      _attributes: {
        returnType: string;
      };
    };
    'query:Query': {
      _attributes: {
        queryDefinition: string;
      };
      'rim:Slot': PersonSlot[] | any;
    };
  };
}

export interface Payload {
  payload: PayloadContent;
}

export interface PayloadContent {
  _attributes: {
    payloadId: string;
    contentType: string;
  };
  value: {
    _text: string;
  };
}

/**
 * OOTS Query Response
 */
export interface OOTSQueryResponse {
  'query:QueryResponse': {
    _attributes: {
      // DYNAMIC
      requestId: string;
      status:
        | 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'
        | 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'
        | 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable';

      // STATIC
      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance';
      'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0';
      'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0';
      'xmlns:sdg': 'http://data.europa.eu/p4s';
      'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0';
      'xmlns:xlink': 'http://www.w3.org/1999/xlink';
      'xmlns:xml': 'http://www.w3.org/XML/1998/namespace';
    };
    'rim:Slot': [
      // SpecficationIdentifier
      {
        _attributes: {
          name: 'SpecificationIdentifier';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType';
          };
          'rim:Value': {
            _text: 'oots-edm:v1.0';
          };
        };
      },
      // EvidenceResponseIdentifier
      {
        _attributes: {
          name: 'EvidenceResponseIdentifier';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType';
          };
          'rim:Value': {
            _text: string;
          };
        };
      },
      // IssueDateTime
      {
        _attributes: {
          name: 'IssueDateTime';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType';
          };
          'rim:Value': {
            _text: string;
          };
        };
      },
      // EvidenceProvider
      {
        _attributes: {
          name: 'EvidenceProvider';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType';
          };
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType';
            };
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: string;
                };
                _text: string;
              };
              'sdg:Name': {
                _attributes: {
                  lang: string;
                };
                _text: string;
              }[];
              'sdg:Classification': {
                _text: string;
              };
              'sdg:Address'?: {
                'sdg:FullAddress'?: {
                  _text: string;
                };
                'sdg:Thoroughfare'?: {
                  _text: string;
                };
                'sdg:LocatorDesignator'?: { _text: string };
                'sdg:AdminUnitLevel1'?: { _text: string };
                'sdg:AdminUnitLevel2'?: { _text: string };
                'sdg:PostCode'?: { _text: string };
                'sdg:PostCityName'?: { _text: string };
              };
            }[];
          };
        };
      },
      // EvidenceRequester
      {
        _attributes: {
          name: 'EvidenceRequester';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType';
          };
          'sdg:Agent': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: string;
              };
              _text: string;
            };
            'sdg:Name': {
              _attributes: {
                lang: string;
              };
              _text: string;
            }[];
          };
        };
      },
      // ResponseAvailableDateTime
      {
        _attributes: {
          name: 'ResponseAvailableDateTime';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType';
          };
          'rim:Value': {
            _text: string;
          };
        };
      }?,
    ];
    'rim:AviResponse'?: any;
    'rim:RegistryObjectList'?: {
      'rim:RegistryObject': RegistryObject[];
    };
  };
}

/**
 * Registry object
 */
export interface RegistryObject {
  _attributes: {
    'xsi:type': 'rim:ExtrinsicObjectType';
    id: string;
  };
  'rim:Slot': {
    _attributes: {
      name: 'EvidenceMetadata';
    };
    'rim:SlotValue': {
      _attributes: {
        'xsi:type': 'rim:AnyValueType';
      };
      'sdg:Evidence': {
        'sdg:Identifier': {
          _text: string;
        };
        'sdg:IssuingDate': {
          _text: string;
        };
        'sdg:IsAbout':
          | {
              'sdg:NaturalPerson': {
                'sdg:Identifier'?: {
                  _attributes: {
                    schemeID: string;
                  };
                  _text: string;
                };
                'sdg:FamilyName': {
                  _text: string;
                };
                'sdg:GivenName'?: {
                  _text: string;
                };
                'sdg:DateOfBirth': {
                  _text: string;
                };
              };
            }
          | {
              'sdg:LegalPerson': {
                'sdg:LegalPersonIdentifier': {
                  _attributes: {
                    schemeID: string;
                  };
                  _text: string;
                };
                'sdg:LegalName': {
                  _text: string;
                };
              };
            };
        'sdg:IssuingAuthority': {
          'sdg:Identifier': {
            _attributes: {
              schemeID: string;
            };
            _text: string;
          };
          'sdg:Name': {
            _attributes: {
              lang: string;
            };
            _text: string;
          }[];
        };
        'sdg:IsConformantTo': {
          'sdg:EvidenceTypeClassification': {
            _text: string;
          };
          'sdg:Title': {
            _attributes: {
              lang: string;
            };
            _text: string;
          }[];
          'sdg:Description'?: {
            _attributes: {
              lang: string;
            };
            _text: string;
          }[];
        };
        'sdg:Distribution': {
          'sdg:Format': {
            _text: string;
          };
          'sdg:Language'?: {
            _text: string;
          }[];
          'sdg:ConformsTo'?: {
            _text: string;
          }[];
          'sdg:Transformation'?: {
            _text: string;
          };
        };
        'sdg:ValidityPeriod': {
          'sdg:StartDate': {
            _text: string;
          };
          'sdg:EndDate': {
            _text: string;
          };
        };
      };
    };
  };
  'rim:RepositoryItemRef': {
    _attributes: {
      'xlink:href': string;
      'xlink:title': string;
    };
  };
}

/**
 * Person slot
 */
export interface PersonSlot {
  _attributes: {
    name: 'NaturalPerson';
  };
  'rim:SlotValue': {
    _attributes: {
      'xsi:type': 'rim:AnyValueType';
    };
    'sdg:Person': {
      'sdg:LevelOfAssurance': {
        _text: string;
      };
      'sdg:Identifier'?: {
        attributes: {
          schemeID: string;
        };
        $value: string;
      };
      'sdg:FamilyName': {
        _text: string;
      };
      'sdg:GivenName': {
        _text: string;
      };
      'sdg:DateOfBirth': {
        _text: string;
      };
      'sdg:BirthName'?: {
        _text: string;
      };
      'sdg:PlaceOfBirth'?: {
        _text: string;
      };
      'sdg:CurrentAddress'?: Address;
      'sdg:SectorSpecificAttribute'?: SectorSpecificAttribute[];
    };
  };
}

interface SectorSpecificAttribute {
  'sdg:AttributeName': string;
  'sdg:AttributeURI': string;
  'sdg:AttributeValue': string;
}

interface Address {
  'sdg:FullAddress'?: string;
  'sdg:Thoroughfare'?: string;
  'sdg:LocatorDesignator'?: string;
  'sdg:AdminUnitLevel1'?: string;
  'sdg:AdminUnitLevel2'?: string;
  'sdg:PostCode'?: string;
  'sdg:PostCityName'?: string;
}

export interface OOTSQueryResponseError {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryResponse': {
    _attributes: {
      'xmlns:xsi': string;
      'xmlns:rs': string;
      'xmlns:sdg': string;
      'xmlns:rim': string;
      'xmlns:query': string;
      'xmlns:xlink': string;
      status: string;
      'xmlns:xml': string;
      requestId: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          _text: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
      };
    }[];
    'rs:Exception': {
      _attributes: {
        'xsi:type':
          | 'rs:AuthorizationExceptionType'
          | 'query:QueryExceptionType';
        severity: 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired';
        message: string;
        code: string;
        detail?: string;
      };
      'rim:Slot': {
        _attributes: {
          name: string;
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': string;
            collectionType?: string;
          };
          'rim:Value'?: any;
          'rim:Element'?: any;
          'sdg:Agent'?: any;
        };
      }[];
    };
  };
}
