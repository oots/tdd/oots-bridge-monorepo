# Demos

This directory contains video demos of some of the flows in the OOTS Bridge project.

## Full flow demo

File: `DEMO_FULL_FLOW_NETHERLANDS_TEST_EMP.mkv`

Description: This video shows a successfull flow, starting from the Procedure Portal, going through the Bridge, approving sharing of evidence at the Netherlands Emrex data provider, and finally receiving the evidence back in the Procedure Portal.

## Timeout demo

File: `DEMO_TIMEOUT.mkv`

Description: This video shows a flow that times out. User arrives at the Bridge after the request has expired. The Bridge sends an OOTS error response back to the user.

## Identity verification demo

File: `DEMO_IDENTITY_MATCHING_ERROR.mkv`

Description: This video shows a flow where the user's identity does not match the identity in the ELMO data received from the Emrex data provider. The Bridge sends an OOTS error response back to the user.


## Emrex XML Validation Error

File: `DEMO_XML_VALIDATION.mkv`

Description: This video shows a flow where the XML received from the Emrex data provider is not valid. The Bridge sends an OOTS error response back to the user.

## Procedure Portal Support Emrex Clients

File: `DEMO_PROCEDURE_PORTAL_AS_EMREX_PROVIDER.mkv`

Description: This video shows a flow where the Procedure Portal acts as an Emrex data provider. This way Emrex clients can request data from the Procedure Portal which then gets the data from OOTS.
