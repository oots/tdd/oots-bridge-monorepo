# OOTS Bridge monorepo

This repository contains all the code for the OOTS Bridge project. The project is a monorepo, which means that it contains multiple packages that are all managed together. The packages are located in the `apps` and `libs` directories.

## Demos

Video demos of some of the flows can be found in the [demos](demos/README.md) directory.

## Documentation

Documentation for the project can be found in the [docs](docs/README.md) directory.

## Apps

- [Bridge frontend](apps/bridge/frontend/README.md)
- [Bridge backend](apps/bridge/backend/README.md)
- [Procedure portal backend](apps/procedure-portal/backend/README.md)
- [Procedure portal frontend](apps/procedure-portal/frontend/README.md)

## Libraries

- [Utils](libs/utils/README.md)

## Tools and libraries used

- [Node.js](https://nodejs.org/)
- [Pnpm](https://pnpm.io/)
- [TypeScript](https://www.typescriptlang.org/)
- [Next.js](https://nextjs.org/)
- [Fastify](https://www.fastify.io/)
- [Nx](https://nx.dev/)
- [Biome](https://biomejs.dev/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
