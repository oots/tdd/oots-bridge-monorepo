FROM node:20.18.0-alpine3.19

WORKDIR /app

# Install pnpm
RUN npm i -g pnpm@9.12.2

# Copy root package.json + pnpm-lock.yaml + pnpm-workspace.yaml
COPY pnpm-lock.yaml package.json pnpm-workspace.yaml .npmrc ./

# Copy package.json for each project

# Utils
COPY ./libs/utils/package.json ./libs/utils/

# Bridge
COPY ./apps/bridge/backend/package.json ./apps/bridge/backend/
COPY ./apps/bridge/frontend/package.json ./apps/bridge/frontend/

# Procedure portal
COPY ./apps/procedure-portal/backend/package.json ./apps/procedure-portal/backend/
COPY ./apps/procedure-portal/frontend/package.json ./apps/procedure-portal/frontend/

# Remove prepare script from root package.json
RUN npm pkg delete scripts.prepare

# Install all the dependencies
RUN pnpm install --frozen-lockfile

# Copy all other files
COPY . .

ARG NEXT_PUBLIC_BASE_PATH_BRIDGE=""
ARG NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL=""

# Set environment variables
ENV NEXT_PUBLIC_BASE_PATH_BRIDGE=$NEXT_PUBLIC_BASE_PATH_BRIDGE
ENV NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL=$NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL
ENV NODE_ENV=production

# For Frontends to be built as standalone
ENV DOCKER_BUILD=true

# Build all projects
RUN pnpm build
