# Docmentation for the OOTS Bridge

Documents:

- [Architecture](architecture.md)
- [Environment variables](env-variables.md)
- [Docker build process](docker-build.md)
