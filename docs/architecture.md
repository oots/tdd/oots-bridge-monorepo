# Architecture

This document contains the architecture and design documentation for the OOTS Bridge project. For a more development focused documentation with instructions on how to run the project, see the [README.md](../README.md) file.

## Flow

For eaiser understanding of the project and how all the parts fit together, we will explain things using the following sequence diagram. It specifies an expected flow of events in the system.

![Sequence diagram](sequence-diagram.png)

### Sequence diagram legend

#### Entities

- **User**: The end user who is trying to access their diploma from a different country.
- **Education portal**: The portal requesting the diploma from the user.
- **Intermediary portal**: The portal that is responsible for handling the communication between the education portal and the Bridge.
- **EB/DSD registry**: The registry that contains the configuration metadata for intermediary portal. It is responsible for providing the supported endpoints and countries for the intermediary portal.
- **Domibus access points**: The communication channel between the Bridge and the university portal. It is responsible for delivering the user's requests to the Bridge and the responses back to the university portal.
- **Bridge frontend**: The frontend part of the Bridge that is responsible for handling the user's requests. It is responsible for redirecting the user to the correct university portal.
- **Bridge backend**: The backend part of the Bridge that is responsible for handling the communication between the Bridge and the university portal. It is responsible for processing the user's requests received from the Domibus access point.
- **Bridge database**: The database that is responsible for storing the state information temporarily. Currently, an in-memory database is employed, which implies that the data is not retained between system reboots.
- **University portal**: The portal that is responsible for providing the diploma to the user. Currently all data is in the ELMO format.

#### Colors

- **Blue**: Represents the user's actions.
- **Green**: Represents the front-channel redirects.
- **Black**: Represents the back-channel communication.

### Steps
| Steps | Description |
| --- | --- |
| 1 | The user visits the education portali of country 1. |
| 2 | The education portal requests the user to authenticate using eIDAS. |
| 3 | The user completes the authentication process. |
| 4 | The eIDAS authentication system returns the identity information to the education portal. |
| 5 - 6 | The education portal redirects the user to the intermediary portal of country 1. |
| 7 - 8 | The intermediary portal gets configuration metadata from the EB/DSD registry. This is where the supported endpoints and countries are set up and prepared.|
| 9 | The user selects the country where their diplomas are located.|
| 10  | The user consents to the use of the OOTS system.|
| 11  | The intermediary portal sends the first OOTS Query request to Domibus access point of country 1.|
| 12  | The Domibus access point delivers the query request to the Bridge access point.|
| 13  | The Bridge backend service is pooling the Bridge access point for new messages. The Bridge backend service receives the first OOTS query request.|
| 14  | The Bridge backend service creates a new unique stateId that will be used to process the following request.|
| 15  | The Bridge backend stores state information temporarily in a database. Currently, an in-memory database is employed, which implies that the data is not retained between system reboots.  |
| 16 | The Bridge backend then processes the OOTS query request and sends back a OOTS query response. This response contains the PreviewLocation attribute, which defines where the user needs to visit to finalize the preview request.  |
| 17 - 18 | The query response is delivered to the intermediary portal. |
| 19 - 21 | The intermediary portal creates and sends a second OOTS query request. This query request also contains the PreviewLocation received in the previous query response and the user selected university portal. |
| 22 - 23 | In parallel to the second query request being sent, the user is redirected to the Bridge frontend service. |
| 24 - 28 | The Bridge frontend does a state lookup to the Bridge backend using the stateId provided in the redirect URL. The Bridge also waits for the second OOTS request before continuing. The state is then validated and updated with the newly provided information (returnUrl, etc.). |
| 29 - 30 | Based on the received state information from the Bridge Backend, the user is redirected to the correct university portal. The redirect is an HTTPS POST redirect, containing the stateId and returnUrl. The returnUrl is a specific endpoint of the Bridge backend service, and it represents the location where the user will be returned after completing the request on the university portal side (successfully or not). |
| 31 - 34 | These steps present the user's actions on the university portal's side. The user authenticates themselves and the requested data, namely the diploma, is obtained from the system. All collected data is packed as ELMO. |
| 35 - 36 | The user is redirected to the returnUrl using the HTTP POST method. The redirect includes the stateId and the data in the ELMO format. |
| 37 | The Bridge backend receives the information via the redirect and decodes the ELMO format. |
| 38 | The Bridge backend performs identity validation to verify that the received data belongs to the requesting user. The matching of entities is done using the Levenshtein distance metric with a predefined maximum threshold. |
| 39 - 41 | The decoded data is sent through Domibus, OOTS query response, back to the intermediary portal. The data is in XML or PDF, based on the information specified in the OOTS query request. |
| 42 | The intermediary portals send the evidence (diploma) to the education portal. |
| 4 3- 46 | In parallel to sending back the OOTS query response, the user is redirected back to the intermediary portal and then again back to the education portal. |
| 47 | An OOTS error response is sent back to the intermediary portal if the identity validation fails.|
| 48 | If the recieved ELMO data from the Emrex provider is not signed or the signature is invalid or the XML does not validate against the XSD, a OOTS error response is sent back to the intermediary portal.|
| 49 | If EMREX returns and error or exceeds the timeout period, a OOTS error response is sent back to the intermediary portal.|
| 50 | OOTS error response is sent back when the state validation fails (missing state, timeout).|
| 51 | OOTS error response is sent back when the request validation fails.|
| 52 | OOTS error response is sent back when the request validation fails.|

### Additional architecture details

- Redirect in step 29 to 30 is defined by the [Emrex specification](https://github.com/emrex-eu/implementation-guide?tab=readme-ov-file#42sending-a-request-to-the-emp). The specification defines that the redirect to the selected EMREX Data Access Point (EMP) needs to be of the `application/x-www-form-urlencoded` type and cointain the `stateId` and `returnUrl` parameters.
- Redirect in step 35 to 36 is defined by the [Emrex specification](https://github.com/emrex-eu/implementation-guide?tab=readme-ov-file#43receiving-a-response). The specification defines that the redirect back to the Bridge backend service will be a HTTPS POST redirect and it contain an EMP return code defining if everything went well or not. A successfull response will also contain the data in ELMO format.

### Error handling

- First OOTS Request
  - Structural XML validation
    - Technical validation -> Ingress and egress XML verification against XSD
    - Business validation -> Validation against Schematron definitions
    - Technical validation (eDelivery) -> Validation against eDelivery Schematron
    - Returned exception -> OOTS Error response with `InvalidRequestExceptionType`
  - Provider validation
    - Verifying if the requested evidence provider is a configured Emrex provider
    - Returned exception -> OOTS Error response with `AuthorizationExceptionType`
- Second OOTS Request
  - Structural XML validation
    - Same as above with `InvalidRequestExceptionType`
  - State validation
    - Verify the extracted stateId from the redirect previewUrl exists in the database
    - Not found or timeout (request is recieved after the timeout perioid - expired request)
    - Returned exception -> OOTS Error response with `InvalidRequestExceptionType`
  - Timeout
    - State information is expired
    - No exception is returned as we didn't receive any request yet. In case we do, refer to the previous point.
  - Failed redirection (timeout, no response recieved)
    - If no response is received from the Emrex provider within a certain time frame (timeout)
    - Returned exception -> OOTS Error response with `rs:TimeoutExceptionType`
  - Emrex error response (EMP_ERROR)
    - Returned exception -> OOTS Error response with `query:QueryExceptionType`
  - Emrex signature validation
    - If the recieved ELMO data from the Emrex provider is not signed or the signature is invalid
    - Returned exception -> OOTS Error response with `query:QueryExceptionType`
  - Emrex data validation
    - Recieved ELMO data from the Emrex provider is validated against the ELMO XSD
    - Returned exception -> OOTS Error response with `query:QueryExceptionType`
  - Identity matching
    - The user's identity is validated against the recieved ELMO data from the Emrex provider. The indetity matching is done using the Levenshtein distance metric with a predefined maximum threshold. If the specified threshold is exceeded, the request is considered invalid.
    - Returned exception -> OOTS Error response with `AuthorizationExceptionType`

#### Additional notes

- State not found or timeout
  - If possible a display warning is shown to the user on the Bridge Frontend
  - If possible a button to go back to the ReturnUrl is shown to the user on the Bridge Frontend
- Emrex responses (EMP_OK, EMP_NO_RESULTS, EMP_CANCEL) are not considered errors and are handled as successful responses. In case of EMP_CANCEL and EMP_NO_RESULTS, the user is redirected back to the ReturnUrl and an empty response is sent to the intermediary portal.


## Additional implementation details

- Domibus is checked for new messages using a pooling interval of 5 seconds.
- A cron job is executed daily at 01:00 to purge the storage. This means it will delete all temporary stored data from finished or timed out requests throughout the day.
- A cron job is executed every 5 minutes to check for timed out requests. If a request was not completed within the specified time frame, it will be marked as timed out and if possible the system sends back and OOTS error response.
- The [fast-xml-parser](https://www.npmjs.com/package/fast-xml-parser) library is used to transform incoming XML requests into JSON. Every action performed within the bridge utilizes JSON, which is subsequently converted back to XML, before sending a response, using the xml-js library.
- An in-memory datastore is used to store the data.
- The [toad-scheduler](https://github.com/kibertoad/toad-scheduler) is used to schedule tasks every 5 seconds. First, the Domibus access point is checked for new messages, then all messages are processed.
- Body limit is increased to 10 MiB as some ELMO files can be quite large, becuase of embedded PDFs.
- Logs are written to STDOUT.
- The Bridge also supports OOTS Evidence requests that request the evidence format `application/json` as it uses a ELMO to ELM converter service. For this to work the `ELMO_CONVERTER_URL` environment variable needs to be set.

## Logging

The Bridge backend uses 2 logger instances for logging. 
- The first logger is a OOTS specific logger, that writes logs defined by the [OOTS Logging specification](https://ec.europa.eu/digital-building-blocks/sites/pages/viewpage.action?pageId=706382155)
- The second logger is a general logger (APP specific) that writes various logs related to the application itself (e.g. startup logs, error logs, etc.)
