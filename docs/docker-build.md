# Document explaining the docker build process

**NOTE:** This document does not explain how to run and setup Domibus, but assumes that you have already done so. If you want to run the Full demo, you will need two Domibus instances, one for the Bridge and one for the Procedure Portal. If you want to run the Bridge only, you will need one Domibus instance.

## Docker images

Built docker images are available on the Code Europa GitLab [container registry](https://code.europa.eu/oots/tdd/oots-bridge-monorepo/container_registry).

## Local docker build

### Requirements

We tested the build process using the following versions:

- Docker 26.1.1
- Docker Compose 2.27.0

### Structure

The project is developed using a monorepo structure. The main builder Docker image is found in the root directory, and it's used to create all the other images. Moreover, each app also has its own docker image.

### Build scripts

The build scripts make the docker builds easier and are located in the `scripts` directory. To build all needed docker images, run the following command:

```sh
./scripts/docker_build_monorepo.sh <base_path_bridge> <base_path_procedure_portal>
```

Where `<base_path_bridge>` and `<base_path_procedure_portal>` are the base paths for the Bridge and Procedure Portal frontends, respectively. This builds the images for the Monorepo and all the apps, and tags them with the latest tag.

For example, if you want to build the images with the base paths `/bridge` and `/procedure-portal`, you would run the following command:

```sh
./scripts/docker_build_monorepo.sh /bridge /procedure-portal
```

If you want to build the images without the base paths, you can run the following command:

```sh
./scripts/docker_build_monorepo.sh
```

This will build the images with the base paths `/` and `/`.

### Build script used for publishing

For publishing the images to the Code Europa GitLab container registry, we use the `docker_build_multiplatform.sh` script. This script builds the images for the specified platforms (`linux/amd64`,`linux/arm64`) and tags them with the specified tag. It also pushes the images to the Code Europa GitLab container registry.

Script usage:

```sh
./scripts/docker_build_multiplatform.sh <tag> <base_path_bridge> <base_path_procedure_portal>
```

Where `<tag>` is the tag for the images, and `<base_path_bridge>` and `<base_path_procedure_portal>` are the base paths for the Bridge and Procedure Portal frontends, respectively.

For example, if you want to build the images with the tag `v1.0.0` and the base paths `/bridge` and `/procedure-portal`, you would run the following command:

```sh
./scripts/docker_build_multiplatform.sh v1.0.0 /bridge /procedure-portal
```

If you want to build the images without the base paths, you can run the following command:

```sh
./scripts/docker_build_multiplatform.sh v1.0.0
```

This will build the images with the base paths `/` and `/`.

## Docker Compose

The project also includes two `docker-compose.yml` files, which can be used to run the project.

### Full demo (Docker Compose)

To run the full demo setup (Bridge and Procedure Portal), first set up the environment variables in the `docker-compose.yml` file. Then run the following command:

```sh
docker-compose up
```

Or to run in detached mode:
```sh
docker-compose up -d
```

This will start the Bridge and Procedure Portal backends, as well as the Bridge and Procedure Portal frontends. The services will be available on the following ports:

- Bridge backend: 3003
- Bridge frontend: 3001
- Procedure portal backend: 3002
- Procedure portal frontend: 3000

### Notes

Both the Bridge and the Procedure Portal backend ports need to be accessible from the outside if run inside a Docker container (not using localhost). This means that the `NEXT_PUBLIC_BRIDGE_BACKEND_URL`and `NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL` environment variables need to be set to the correct URLs. If you are running the demo on a local machine, you can use something like [Ngrok](https://ngrok.com/) or [Zrok](https://zrok.io/) to expose the services.

Example Ngrok setup:

```sh
# First set the `authtoken` variable inside the `ngrok.yml` file
# then run the following command (in the root directory)
ngrok start --all --config ngrok.yml
```

### Bridge only (Docker Compose)

To run only the Bridge, first set up the environment variables in the `docker-compose.bridge.yml` file.

Then run the following command:

```sh
docker-compose -f docker-compose.bridge.yml up
```

Or to run in detached mode:
```sh
docker-compose -f docker-compose.bridge.yml up -d
```

This will start the Bridge backend and Bridge frontend. The services will be available on the following ports:

- Bridge backend: 3003
- Bridge frontend: 3001
