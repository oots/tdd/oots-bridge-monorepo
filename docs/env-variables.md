# Document explaining the environment variables

## Services

### Bridge backend

| Variable | Description | Required | Default |
| --- | --- | --- | --- |
| BRIDGE_FRONTEND_URL | URL of where the Bridge frontend will be hosted | Yes | |
| PARTY_ID | Party ID of the Bridge backend | Yes ||
| DOMIBUS_ACCESS_POINT | URL of the Domibus access point that should be used by the Bridge backend | Yes ||
| PROXY_URL | URL of the forward proxy server | No ||
| ELM_CONVERTER_URL | URL Where the ELM converter is hosted. If not set, the ELMO to ELM conversion will not work | No ||
| EMREX_DATA_PROVIDERS | JSON object with EMREX data providers | Yes ||
| TIMEOUT_IN_SECONDS | Timeout in seconds when a timeout exception is returned | No | 3600 |
| LEVENSHTEIN_THRESHOLD | Levenshtein threshold for identity matching | No | 0 |

### Bridge frontend

| Variable | Description | Required |
| --- | --- | --- |
| NEXT_PUBLIC_BRIDGE_BACKEND_URL | URL of where the Bridge backend will be hosted | Yes |
| NEXT_PUBLIC_BASE_PATH_BRIDGE | With this variable you can set a base path for the Bridge frontend. This is useful if you want to host the frontend on a subpath of your domain, instead of a subdomain, for example: https://example.com/bridge | No |

### Demo procedure portal backend


| Variable | Description | Required |
| --- | --- | --- |
| DOMIBUS_ACCESS_POINT | URL of the Domibus access point that should be used by the Procedure Portal backend | Yes |
| ACCESS_POINT_PARTY_ID | Party ID of the Domibus access point | Yes |
| ACCESS_POINT_PARTY_TYPE | Party type of the Domibus access point | Yes |
| PARTY_ID | Party ID of the Procedure Portal backend | Yes |
| BRIDGE_ACCESS_POINT_PARTY_ID | Party ID of the Bridge access point where messages should be sent | Yes |
| BRIDGE_ACCESS_POINT_PARTY_TYPE | Party type of the Bridge access point where messages should be sent | Yes |
| BRIDGE_PARTY_ID | Party ID of the Bridge backend (final recipient) | Yes |
| PROCEDURE_PORTAL_FRONTEND_URL | URL of the Procedure Portal frontend | Yes |
| EMREX_DATA_PROVIDERS | JSON object with EMREX data providers | Yes |

### Demo procedure portal frontend

| Variable | Description | Required |
| --- | --- | --- |
| NEXT_PUBLIC_PROCEDURE_PORTAL_BACKEND_URL | URL of where the Procedure Portal backend will be hosted | Yes |
| NEXT_PUBLIC_PROCEDURE_PORTAL_FRONTEND_URL | URL of where the Procedure Portal frontend will be hosted | Yes |
| NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL | With this variable you can set a base path for the Procedure Portal frontend.
