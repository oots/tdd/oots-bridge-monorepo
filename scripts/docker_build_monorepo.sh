#!/bin/bash

# This script is used to build the docker images for the Monorepo
docker build --build-arg="NEXT_PUBLIC_BASE_PATH_BRIDGE=$1" --build-arg="NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL=$2" -t oots-emrex/monorepo:latest .

# Build Bridge Backend
docker build -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-backend:latest apps/bridge/backend
docker build -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-frontend:latest apps/bridge/frontend

# Build Procedure Portal Backend
docker build -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-backend:latest apps/procedure-portal/backend
docker build -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-frontend:latest apps/procedure-portal/frontend

