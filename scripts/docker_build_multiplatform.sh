#!/bin/bash

TAG=$1

if [ -z "$TAG" ]; then
  echo "Usage: $0 <tag>"
  exit 1
fi

# This script is used to build the docker images for the Monorepo
docker buildx build --provenance=false --platform linux/amd64,linux/arm64 --build-arg="NEXT_PUBLIC_BASE_PATH_BRIDGE=$2" --build-arg="NEXT_PUBLIC_BASE_PATH_PROCEDURE_PORTAL=$3" -t oots-emrex/monorepo:latest .

# Build Bridge Backend
docker buildx build --provenance=false --push --platform linux/amd64,linux/arm64 -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-backend:$TAG apps/bridge/backend
docker buildx build --provenance=false --push --platform linux/amd64,linux/arm64 -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-frontend:$TAG apps/bridge/frontend

# Build Procedure Portal Backend
docker buildx build --provenance=false --push --platform linux/amd64,linux/arm64 -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-backend:$TAG apps/procedure-portal/backend
docker buildx build --provenance=false --push --platform linux/amd64,linux/arm64 -t code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-frontend:$TAG apps/procedure-portal/frontend
