#!/bin/bash

TAG=$1

if [ -z "$TAG" ]; then
  echo "Usage: $0 <tag>"
  exit 1
fi

docker tag code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-backend:latest code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-backend:$TAG
docker tag code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-frontend:latest code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/bridge-frontend:$TAG

docker tag code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-backend:latest code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-backend:$TAG
docker tag code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-frontend:latest code.europa.eu:4567/oots/tdd/oots-bridge-monorepo/procedure-portal-frontend:$TAG
